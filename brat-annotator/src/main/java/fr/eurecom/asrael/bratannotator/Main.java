package fr.eurecom.asrael.bratannotator;

import fr.eurecom.asrael.commons.utils.Configuration;
import java.io.IOException;

public class Main {

  public static void main(final String... args) {
    try {
      new BratAnnotator(Configuration.load()).run();
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }
}
