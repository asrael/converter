package fr.eurecom.asrael.bratannotator;

import com.pengyifan.brat.BratAttribute;
import com.pengyifan.brat.BratDocument;
import com.pengyifan.brat.BratEntity;
import com.pengyifan.brat.io.BratDocumentReader;
import fr.eurecom.asrael.commons.datatypes.Annotation;
import fr.eurecom.asrael.commons.datatypes.NewsArticle;
import fr.eurecom.asrael.commons.datatypes.WikidataEvent;
import fr.eurecom.asrael.commons.utils.Configuration;
import fr.eurecom.asrael.commons.utils.UUIDGenerator;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.io.FilenameUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BratAnnotator {
  private static final Logger LOGGER = LoggerFactory.getLogger(BratAnnotator.class);
  private final Configuration config;
  private HashMap<String, String> propertiesLabelsMap;

  public BratAnnotator(Configuration config) {
    this.config = config;
    this.propertiesLabelsMap = new HashMap<>();
  }

  public void run() {
    final Path outputDir = Paths.get(config.getDumpPath(), "annotations");
    // Make sure that the dump directory exists
    try {
      Files.createDirectories(outputDir);
    } catch (IOException e) {
      LOGGER.error(e.toString());
    }

    // Read schemas file
    JSONObject data = null;
    try {
      data =
          new JSONObject(
              Files.readAllLines(Paths.get(config.getSchemasPath())).stream()
                  .collect(Collectors.joining()));
    } catch (JSONException | IOException e) {
      BratAnnotator.LOGGER.error("Error when parsing schemas: " + e.toString());
    }
    Iterator<String> keys = data.keys();
    while (keys.hasNext()) {
      String key = keys.next();
      JSONObject schemaObj = (JSONObject) data.get(key);
      JSONArray propertiesArray = (JSONArray) schemaObj.get("properties");
      for (int i = 0; i < propertiesArray.length(); i++) {
        JSONObject propertyObj = propertiesArray.getJSONObject(i);
        propertiesLabelsMap.put(
            propertyObj.getString("property_ID"), propertyObj.getString("property"));
      }
    }

    try {
      Files.list(Paths.get(config.getResourcesPath(), "annotations"))
          .filter(file -> file.toFile().isFile() && file.toFile().toString().endsWith(".ann"))
          .forEach(
              file -> {
                WikidataEvent event = convertAnnotationsFile(file);
                if (event == null) {
                  BratAnnotator.LOGGER.warn("WikidataEvent is null for " + file.toString());
                  return;
                }

                final Model finalModel = ModelFactory.createDefaultModel();

                // Add namespace to the final model
                finalModel.setNsPrefix("asrael", "http://asrael.eurecom.fr/asrael#");
                finalModel.setNsPrefix("rnews", "http://iptc.org/std/rNews/2011-10-07#");

                finalModel.add(event.getModel());

                // Write the model to a turtle file
                final String baseName = FilenameUtils.getBaseName(file.toString());
                Path outFile =
                    Paths.get(
                        outputDir.toString(),
                        FilenameUtils.getBaseName(file.toString()) + ".ann.ttl");
                BratAnnotator.LOGGER.info("Writing brat annotations to file {}", outFile);
                try (final OutputStream outRdf =
                    Files.newOutputStream(
                        outFile, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING)) {
                  RDFDataMgr.write(outRdf, finalModel, RDFFormat.TURTLE_PRETTY);
                } catch (final IOException e) {
                  BratAnnotator.LOGGER.error("Issue to write brat annotated file", e);
                }
              });
    } catch (IOException e) {
      BratAnnotator.LOGGER.error(e.toString());
    }
  }

  private WikidataEvent convertAnnotationsFile(final Path file) {
    BratAnnotator.LOGGER.info("Converting " + file.toString());

    WikidataEvent event = new WikidataEvent();
    String identifier = null;

    BratDocumentReader bdr = null;
    BratDocument bd = null;
    try {
      final String result = Files.readAllLines(file).stream().collect(Collectors.joining("\n"));
      Reader bratReader = new StringReader(result);
      bdr = new BratDocumentReader(new BufferedReader(bratReader));
      bd = bdr.read();
      bdr.close();
      bratReader.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    List<BratEntity> bratEntities = bd.getEntities();
    for (BratEntity ent : bratEntities) {
      BratAnnotator.LOGGER.debug("--------------------");
      BratAnnotator.LOGGER.debug(ent.toString());
      BratAnnotator.LOGGER.debug("Text: " + ent.getText());
      BratAnnotator.LOGGER.debug("Type: " + ent.getType());

      BratAnnotator.LOGGER.debug("Attributes:");
      List<BratAttribute> bratAttributes = bd.getAttributes(ent.getId());
      for (BratAttribute ba : bratAttributes) {
        Set<String> attributes = ba.getAttributes();
        for (String att : attributes) {
          BratAnnotator.LOGGER.debug(">> " + ba.getType() + " = " + att);
          switch (ba.getType()) {
            case "wiki_instance":
              // Same As
              event.setSameAs("http://www.wikidata.org/entity/" + att);
              break;
            case "instance_of":
              // Wikidata category (Q)
              event.setWikidataEntity(att);
              break;
            case "identifier":
              identifier = att;
              break;
            case "Classification":
            case "schema":
              // Ignore "Classification" and "schema"
              break;
            default:
              event.addProperty(ba.getType(), att);
              break;
          }
        }
      }

      if (identifier == null) {
        BratAnnotator.LOGGER.error("No identifier found in annotations from " + file.toString());
        return null;
      }

      final String newsUri = NewsArticle.BASE_URI + UUIDGenerator.generateUUID(identifier);
      event.setId(identifier);
      event.setCanonicalURL(newsUri);

      // Remove creation/modification date because it was already set by classification-converter
      event.setCreatedTime(null);
      event.setModifiedTime(null);

      if (!ent.getType().equals("Classification")) {
        event.addProperty(ent.getType(), ent.getText());

        if (!ent.getSpans().isEmpty()) {
          int startPosition = ent.getSpans().span().lowerEndpoint();
          int endPosition = ent.getSpans().span().upperEndpoint();
          BratAnnotator.LOGGER.debug("Position: " + startPosition + ":" + endPosition);

          Annotation ann = new Annotation();
          ann.setBody(ent.getText());
          ann.setProperty(ent.getType());
          ann.setPropertyLabel(this.propertiesLabelsMap.get(ent.getType()));
          ann.setStartPosition(startPosition);
          ann.setEndPosition(endPosition);
          ann.setSource(newsUri);
          ann.setCanonicalURL(newsUri + "#char=" + startPosition + "," + endPosition);
          event.addAnnotation(ann);
        }
      }
    }

    return event;
  }
}
