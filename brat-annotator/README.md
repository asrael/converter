# BratAnnotator

Annotates RDF turtle files in `data/dump/agencefrancepresse` using brat annotation files in `data/resource/brat/agencefrancepresse`.

## Compilation

```
mvn -U clean package
```

## Configuration

Before running, make sure to copy/rename the file `props/default.yml` into `props/config.yml` and open it to set up the properties:

* **annotatorPath** - path to the `src` folder of the [news-annotations](https://gitlab.eurecom.fr/asrael/news-annotations/) project.\
Example: `annotatorPath=/path/to/news-annotations/src`

* **annotatorFile** - name of the script file used to annotate articles.\
Example: `annotatorFile=annotate_article.py`

## Usage

```
mvn exec:java  -Dlogfile.name=asrael-bratannotator.log -Dlogfile.append=true -pl brat-annotator
```
