package fr.eurecom.asrael.classificationconverter;

import fr.eurecom.asrael.commons.utils.Configuration;
import java.io.IOException;

public class Main {

  public static void main(final String... args) {
    try {
      new ClassificationConverter(Configuration.load()).run();
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }
}
