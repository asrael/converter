package fr.eurecom.asrael.classificationconverter;

import fr.eurecom.asrael.commons.datatypes.NewsArticle;
import fr.eurecom.asrael.commons.datatypes.WikidataEvent;
import fr.eurecom.asrael.commons.utils.Configuration;
import fr.eurecom.asrael.commons.utils.UUIDGenerator;
import fr.eurecom.asrael.newscollector.agencefrancepresse.AgenceFrancePresseMethods;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import org.apache.commons.io.IOUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClassificationConverter {
  private static final Logger LOGGER = LoggerFactory.getLogger(ClassificationConverter.class);
  private final Configuration config;

  public ClassificationConverter(Configuration config) {
    this.config = config;
  }

  public void run() {
    final Model model = ModelFactory.createDefaultModel();

    // Parse JSON
    JSONObject data = null;
    try (FileReader reader =
        new FileReader(String.valueOf(Paths.get(config.getClassificationJsonPath())))) {
      data = new JSONObject(IOUtils.toString(reader));
    } catch (JSONException e) {
      LOGGER.error("Error when parsing schema classification json file: " + e.toString());
    } catch (IOException e) {
      e.printStackTrace();
    }

    if (data == null) {
      return;
    }

    Iterator<String> keys = data.keys();
    while (keys.hasNext()) {
      String key = keys.next();
      JSONObject details = (JSONObject) data.get(key);
      JSONArray schemasArray = (JSONArray) details.get("schemas");

      String newsURI = NewsArticle.BASE_URI + UUIDGenerator.generateUUID(key);

      WikidataEvent event = new WikidataEvent();
      event.setId(key);
      event.setCanonicalURL(NewsArticle.BASE_URI + UUIDGenerator.generateUUID(key));

      model.add(event.getModel());
      model.add(
          ResourceFactory.createResource(newsURI),
          ResourceFactory.createProperty("http://iptc.org/std/rNews/2011-10-07#about"),
          ResourceFactory.createResource(
              WikidataEvent.BASE_URI + UUIDGenerator.generateUUID(event.getCanonicalURL())));

      for (int i = 0; i < schemasArray.length(); i++) {
        String schema = schemasArray.getString(i);
        model.add(
            ResourceFactory.createResource(WikidataEvent.BASE_URI + UUIDGenerator.generateUUID(event.getCanonicalURL())),
            ResourceFactory.createProperty("http://schema.org/category"),
            ResourceFactory.createResource("http://asrael.eurecom.fr/category/" + schema));
      }
    }

    final Path classificationDir = Paths.get(config.getDumpPath(), "classification");

    // Make sure that the dump directory exists
    try {
      Files.createDirectories(classificationDir);
    } catch (IOException e) {
      LOGGER.error(e.toString());
    }

    // Write the model to a turtle file
    DateFormat df = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'");
    final String rdfFile = df.format(new Date()) + ".class.nt";
    LOGGER.info("Writing classifications to file {}", rdfFile);
    Path outFile = Paths.get(classificationDir.toString(), rdfFile);
    try (final OutputStream outRdf =
        Files.newOutputStream(
            outFile, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING)) {
      RDFDataMgr.write(outRdf, model, RDFFormat.NTRIPLES);
    } catch (final IOException e) {
      LOGGER.error("Issue to write classifications to file", e);
    }
  }
}
