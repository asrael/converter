# AdelAnnotator

Runs through RDF turtle files in `data/dump/agencefrancepresse` and annotates them using [ADEL](http://adel.eurecom.fr/api/).

## Compilation

```
mvn -U clean package
```

## Usage

```
java -Dlogfile.name=asrael-adelannotator.log -Dlogfile.append=true -jar target/AdelAnnotator-1.0-SNAPSHOT.jar
```
