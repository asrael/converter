package fr.eurecom.asrael.adelannotator;

import fr.eurecom.asrael.commons.utils.Configuration;
import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.riot.RiotException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AdelAnnotator {
  private static final Logger LOGGER = LoggerFactory.getLogger(AdelAnnotator.class);
  private static final Pattern ADEL_URI_PATTERN =
      Pattern.compile("((?: +)(?:[^ ]+)(?: +)(?:(?!\").)|^)<(.+)>", Pattern.MULTILINE);
  private static final Pattern OPENING_HTML_TAG_PATTERN =
      Pattern.compile("<(?:(?! ))(?:\"[^\"]*\"['\"]*|'[^']*'['\"]*|[^'\">])+>");
  private static final Pattern CLOSING_HTML_TAG_PATTERN =
      Pattern.compile("<\\/(?:(?! ))(?:\"[^\"]*\"['\"]*|'[^']*'['\"]*|[^'\">])+>");
  private final Configuration config;

  public AdelAnnotator(Configuration config) {
    this.config = config;
  }

  public void run() {
    try {
      Files.list(Paths.get(config.getDumpPath(), config.getDatasetName()))
          .filter(file -> file.toFile().isDirectory())
          .forEach(
              langDir -> {
                try {
                  Files.list(langDir)
                      .filter(file -> file.toFile().getName().endsWith(".news.ttl"))
                      .forEach(
                          file -> {
                            final Model model = ModelFactory.createDefaultModel();
                            final Model finalModel = ModelFactory.createDefaultModel();
                            model.add(RDFDataMgr.loadModel(file.toString()));

                            // Add asrael namespace to the final model
                            finalModel.setNsPrefix("asrael", "http://asrael.eurecom.fr/asrael#");

                            // Calculate total rows
                            int totalRows = 0;
                            final String queryCount =
                                "PREFIX rnews: <http://iptc.org/std/rNews/2011-10-07#> "
                                    + "PREFIX asrael: <http://asrael.eurecom.fr/asrael#> "
                                    + "SELECT (COUNT(?s) AS ?nb) WHERE { ?s rnews:articleBody ?desc . MINUS { ?s asrael:context ?context . } }";
                            try (QueryExecution qexecCount =
                                QueryExecutionFactory.create(queryCount, model)) {
                              ResultSet rs = qexecCount.execSelect();
                              while (rs.hasNext()) {
                                QuerySolution binding = rs.nextSolution();
                                Literal nb = binding.getLiteral("nb");
                                totalRows = nb.getInt();
                              }
                            }

                            if (totalRows > 0) {
                              // For each row
                              final String query =
                                  "PREFIX rnews: <http://iptc.org/std/rNews/2011-10-07#> "
                                      + "PREFIX asrael: <http://asrael.eurecom.fr/asrael#> "
                                      + "SELECT ?s ?desc (LANG(?desc) AS ?lang) WHERE { ?s rnews:articleBody ?desc . MINUS { ?s asrael:context ?context . } }";
                              try (QueryExecution qexec =
                                  QueryExecutionFactory.create(query, model)) {
                                ResultSet rs = qexec.execSelect();
                                while (rs.hasNext()) {
                                  QuerySolution binding = rs.nextSolution();
                                  Resource subj = (Resource) binding.get("s");
                                  String uriString = subj.getURI();

                                  AdelAnnotator.LOGGER.info(
                                      "Annotating {} ({}/{})",
                                      uriString,
                                      rs.getRowNumber(),
                                      totalRows);

                                  Literal desc = binding.getLiteral("desc");
                                  String descString = desc.getString();
                                  Literal lang = binding.getLiteral("lang");
                                  String langString = lang.getString();

                                  // Strip HTML tags from description string (for ADEL)
                                  String strippedDescString =
                                      CLOSING_HTML_TAG_PATTERN.matcher(descString).replaceAll(" ");
                                  strippedDescString =
                                      OPENING_HTML_TAG_PATTERN
                                          .matcher(strippedDescString)
                                          .replaceAll("");

                                  // Annotate the description with ADEL
                                  String annotationsString = null;
                                  int tries = 0;
                                  while (tries < 3 && annotationsString == null) {
                                    try {
                                      annotationsString = annotate(strippedDescString, langString);
                                    } catch (final IOException e) {
                                      tries++;
                                      try {
                                        Thread.sleep(1000 * tries);
                                      } catch (InterruptedException e1) {
                                        e1.printStackTrace();
                                      }
                                    }
                                  }
                                  if (annotationsString == null) {
                                    AdelAnnotator.LOGGER.error(
                                        "Could not annotate {} (ADEL returned an error or null)",
                                        uriString);
                                    AdelAnnotator.LOGGER.error(
                                        "Description string: {}", descString);
                                  } else {
                                    final Model annotationsModel =
                                        ModelFactory.createDefaultModel();
                                    try {
                                      annotationsModel.read(
                                          new ByteArrayInputStream(annotationsString.getBytes()),
                                          null,
                                          "TTL");
                                    } catch (final RiotException e) {
                                      AdelAnnotator.LOGGER.error(
                                          "Could not parse RDF content returned by ADEL: {}", e);
                                      AdelAnnotator.LOGGER.error(
                                          "Description string: {}", descString);
                                    }

                                    final String queryContext =
                                        "PREFIX nif: <http://persistence.uni-leipzig.org/nlp2rdf/ontologies/nif-core#> "
                                            + "SELECT ?s WHERE { ?s a nif:Context }";
                                    try (QueryExecution qexec2 =
                                        QueryExecutionFactory.create(
                                            queryContext, annotationsModel)) {
                                      ResultSet rs2 = qexec2.execSelect();
                                      while (rs2.hasNext()) {
                                        QuerySolution binding2 = rs2.nextSolution();
                                        Resource contextSubj = (Resource) binding2.get("s");

                                        // Add the annotations model generated by ADEL, to our
                                        // current model with news articles
                                        finalModel.add(annotationsModel);

                                        // Update news article by adding a link to the annotations
                                        // context
                                        Statement updateStatement =
                                            ResourceFactory.createStatement(
                                                subj,
                                                ResourceFactory.createProperty(
                                                    "http://asrael.eurecom.fr/asrael#", "context"),
                                                contextSubj);
                                        finalModel.add(updateStatement);
                                      }
                                    }
                                  }
                                }
                              }

                              // Overwrite the original turtle file
                              final String baseName = FilenameUtils.getBaseName(file.toString());
                              final String filePath =
                                  FilenameUtils.getPath(file.toString())
                                      + baseName.substring(0, baseName.lastIndexOf(".news"))
                                      + ".adel.ttl";
                              AdelAnnotator.LOGGER.info("Writing annotations to file {}", filePath);
                              Path outFile = Paths.get(filePath);
                              try (final OutputStream outRdf =
                                  Files.newOutputStream(
                                      outFile,
                                      StandardOpenOption.CREATE,
                                      StandardOpenOption.TRUNCATE_EXISTING)) {
                                RDFDataMgr.write(outRdf, finalModel, RDFFormat.TURTLE_PRETTY);
                              } catch (final IOException e) {
                                AdelAnnotator.LOGGER.error("Issue to write annotated file", e);
                              }
                            }
                          });
                } catch (IOException e) {
                  AdelAnnotator.LOGGER.error(e.toString());
                }
              });
    } catch (IOException e) {
      AdelAnnotator.LOGGER.error(e.toString());
    }
  }

  private String annotate(String content, String language) throws IOException {
    String address = "http://adel.eurecom.fr/v1/nerd?setting=aida";
    if (language != null && !language.isEmpty()) {
      address += "&language=" + URLEncoder.encode(language, "UTF-8");
    }

    String result = null;
    HttpURLConnection httpURLConnection;
    try {
      final URL url = new URL(address);
      httpURLConnection = (HttpURLConnection) url.openConnection();

      httpURLConnection.setRequestMethod("POST");
      httpURLConnection.setDoOutput(true);
      httpURLConnection.setRequestProperty("Accept", "text/plain;charset=utf-8");
      httpURLConnection.setRequestProperty("Content-Type", "application/json;charset=utf-8");

      try (final OutputStream outputStream = httpURLConnection.getOutputStream();
          BufferedWriter bufferedWriter =
              new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"))) {

        JSONObject params = new JSONObject();
        params.put("content", content);
        params.put("input", "raw");
        params.put("output", "nif");

        bufferedWriter.write(params.toString());
        bufferedWriter.flush();

        final int responseCode = httpURLConnection.getResponseCode();
        final InputStream inputStream;
        if (responseCode == HttpURLConnection.HTTP_OK) {
          inputStream = httpURLConnection.getInputStream();
        } else {
          inputStream = httpURLConnection.getErrorStream();
        }

        if (inputStream != null) {
          InputStream in = new BufferedInputStream(httpURLConnection.getInputStream());
          result = IOUtils.toString(in, "UTF-8");
        }
      }
    } catch (final IOException e) {
      AdelAnnotator.LOGGER.error(e.toString());
      throw e;
    }

    // Fix URIs encoding
    // (see: https://github.com/D2KLab/adel/issues/14)
    if (result != null) {
      StringBuffer sbFixedResult = new StringBuffer();
      Matcher regexMatcher = ADEL_URI_PATTERN.matcher(result);
      while (regexMatcher.find()) {
        try {
          URI fixedURL = encodeURI(regexMatcher.group(2));
          regexMatcher.appendReplacement(
              sbFixedResult,
              Matcher.quoteReplacement(regexMatcher.group(1) + "<" + fixedURL + ">"));
        } catch (final Exception e) {
          AdelAnnotator.LOGGER.error(e.toString());
        }
      }
      regexMatcher.appendTail(sbFixedResult);
      result = sbFixedResult.toString();
    }

    return result;
  }

  private URI encodeURI(final String str) throws Exception {
    String decodedURL = URLDecoder.decode(str, "UTF-8");
    URL url = new URL(decodedURL);
    return new URI(
        url.getProtocol(),
        url.getUserInfo(),
        url.getHost(),
        url.getPort(),
        url.getPath(),
        url.getQuery(),
        url.getRef());
  }
}
