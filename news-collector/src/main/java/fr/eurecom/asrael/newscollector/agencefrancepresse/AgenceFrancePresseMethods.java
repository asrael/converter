package fr.eurecom.asrael.newscollector.agencefrancepresse;

import fr.eurecom.asrael.commons.datatypes.ImageObject;
import fr.eurecom.asrael.commons.datatypes.NewsArticle;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class AgenceFrancePresseMethods {
  private static final Logger LOGGER = LoggerFactory.getLogger(AgenceFrancePresseMethods.class);

  private AgenceFrancePresseMethods() {}

  public static NewsArticle parseArticle(XMLStreamReader streamReader) {
    NewsArticle news = new NewsArticle();

    StringBuilder description = new StringBuilder();
    boolean inDataContent = false;

    try {
      while (streamReader.hasNext()) {
        if (inDataContent) {
          if (streamReader.hasName()) {
            if (!streamReader.getLocalName().equals("DataContent")
                && !streamReader.getLocalName().equals("body")) {
              description.append("<");
              if (streamReader.isEndElement()) {
                description.append("/");
              }
              description.append(streamReader.getLocalName()).append(">");
            }
          } else if (streamReader.isCharacters()) {
            description.append(streamReader.getText());
          }
        }

        if (streamReader.isStartElement()) {
          switch (streamReader.getLocalName()) {
            case "PublicIdentifier":
              news.setId(streamReader.getElementText());
              break;
            case "FirstCreated":
              news.setDateCreated(parseDate(streamReader.getElementText()));
              break;
            case "ThisRevisionCreated":
              news.setDateModified(parseDate(streamReader.getElementText()));
              break;
            case "DateAndTime":
              news.setDatePublished(parseDate(streamReader.getElementText()));
              break;
            case "NameLabel":
              news.setSlug(streamReader.getElementText());
              break;
            case "DateLine":
              final String dateline = streamReader.getElementText();
              final int pos = dateline.indexOf(",");
              if (pos > -1) {
                final String datelineLocation = dateline.substring(0, pos);
                news.setDateline(datelineLocation);
              }
              break;
            case "HeadLine":
              String headLineLanguage =
                  streamReader.getAttributeValue("http://www.w3.org/XML/1998/namespace", "lang");
              if (headLineLanguage == null) {
                headLineLanguage = news.getLanguage();
              }
              news.addHeadline(headLineLanguage, streamReader.getElementText());
              break;
            case "Subject":
            case "SubjectDetail":
            case "SubjectMatter":
              {
                final String subject = streamReader.getAttributeValue(null, "FormalName");
                if (subject != null) {
                  try {
                    final String encodedSubject = URLEncoder.encode(subject, "UTF-8");
                    news.addSubject("http://cv.iptc.org/newscodes/subjectcode/" + encodedSubject);
                  } catch (final UnsupportedEncodingException ex) {
                    ex.printStackTrace();
                  }
                }
                break;
              }
            case "Genre":
              final String genre = streamReader.getAttributeValue(null, "FormalName");
              if (genre != null) {
                news.setGenre(genre);
              }
              break;
            case "Language":
              news.setLanguage(streamReader.getAttributeValue(null, "FormalName"));
              break;
            case "Property":
              {
                final String formalName = streamReader.getAttributeValue(null, "FormalName");
                final String value = streamReader.getAttributeValue(null, "Value");
                if (value != null) {
                  switch (formalName) {
                    case "Country":
                      news.setCountry(value);
                      break;
                    case "City":
                      news.setCity(value);
                      break;
                    case "Words":
                      // news.setWordsCount(Integer.parseInt(value));
                      break;
                    case "Keyword":
                      news.addKeyword(value);
                      break;
                  }
                }
                break;
              }
            case "DataContent":
              inDataContent = true;
              break;
          }
        }

        if (streamReader.isEndElement()) {
          switch (streamReader.getLocalName()) {
            case "DataContent":
              news.addDescription(news.getLanguage(), description.toString());

              inDataContent = false;
              description = new StringBuilder();
              break;
          }
        }

        streamReader.next();
      }
    } catch (final XMLStreamException e) {
      AgenceFrancePresseMethods.LOGGER.error("Error when parsing XML file: " + e.toString());
    }

    if (news.getId().isEmpty()) {
      return null;
    }

    return news;
  }

  public static List<ImageObject> getArticleImages(NewsArticle article) {
    AgenceFrancePresseMethods.LOGGER.info("Querying Media API for article " + article.getId());

    List<ImageObject> images = new ArrayList<>();

    if (article.getDatePublished().isEqual(LocalDateTime.of(0, Month.JANUARY, 1, 0, 0, 0))) {
      AgenceFrancePresseMethods.LOGGER.info(
          "getArticleImages: Article " + article.getId() + " has no published date");
      return images;
    }
    if (article.getSubjects().isEmpty()) {
      AgenceFrancePresseMethods.LOGGER.info(
          "getArticleImages: Article " + article.getId() + " has no subjects");
      return images;
    }
    if (article.getDateline().isEmpty()) {
      AgenceFrancePresseMethods.LOGGER.info(
          "getArticleImages: Article " + article.getId() + " has no dateline");
      return images;
    }

    try {
      String subjectCode = article.getSubjects().iterator().next();
      if (subjectCode.startsWith("http://cv.iptc.org/newscodes/subjectcode/")) {
        subjectCode = subjectCode.substring("http://cv.iptc.org/newscodes/subjectcode/".length());
      }

      final String apiUrl = "http://medialab.afp.com/afp-4w";
      final URIBuilder uriBuilder = new URIBuilder(apiUrl);
      uriBuilder
          .setParameter(
              "when", article.getDatePublished().format(DateTimeFormatter.ofPattern("YYYY-MM-dd")))
          .setParameter("what_iptc", subjectCode)
          .setParameter("where", article.getDateline())
          .setParameter("lang", article.getLanguage())
          .setParameter("format", "json");

      final URI uri = uriBuilder.build();
      AgenceFrancePresseMethods.LOGGER.debug("Media API url: " + uri.toString());

      final HttpGet req = new HttpGet(uri);

      final HttpClient httpClient = HttpClientBuilder.create().build();
      final HttpResponse res = httpClient.execute(req);

      JSONObject data = null;
      try {
        data = new JSONObject(IOUtils.toString(res.getEntity().getContent(), "UTF-8"));
      } catch (JSONException e) {
        AgenceFrancePresseMethods.LOGGER.error(
            "Error when parsing response from Media API: " + e.toString());
      }
      EntityUtils.consume(res.getEntity());

      if (data != null && data.get("response") != null) {
        final JSONObject response = (JSONObject) data.get("response");
        if (response.get("docs") != null) {
          final JSONArray docs = (JSONArray) response.get("docs");
          if (!docs.isEmpty()) {
            final JSONObject doc = (JSONObject) docs.get(0);
            if (doc != null) {
              final String serial = String.valueOf(doc.get("serial"));
              if (serial != null && !serial.isEmpty()) {
                // Get the list of images for this article
                images = downloadSerial(article, serial);
              }
            }
          }
        }
      }
    } catch (URISyntaxException | IOException e) {
      AgenceFrancePresseMethods.LOGGER.error("Error when querying Media API: " + e.toString());
      e.printStackTrace();
    }

    return images;
  }

  private static List<ImageObject> downloadSerial(final NewsArticle article, final String serial) {
    AgenceFrancePresseMethods.LOGGER.info("Downloading serial " + serial);

    final String url = "http://medialab.afp.com" + serial;
    List<ImageObject> images = new ArrayList<>();

    XMLInputFactory inputFactory = XMLInputFactory.newInstance();
    try (InputStream in = new URL(url).openStream()) {
      XMLStreamReader streamReader = inputFactory.createXMLStreamReader(in);

      boolean inNewsComponent = false;
      boolean inContentItem = false;
      String mediaType = "";
      String caption = "";
      ImageObject tmpImage = null;
      String role = "";

      while (streamReader.hasNext()) {
        if (streamReader.isStartElement()) {
          switch (streamReader.getLocalName()) {
            case "MediaType":
              if (inContentItem) {
                mediaType = streamReader.getAttributeValue(null, "FormalName");
              }
              break;
            case "Property":
              if (inContentItem) {
                final String value = streamReader.getAttributeValue(null, "Value");
                switch (streamReader.getAttributeValue(null, "FormalName")) {
                  case "Width":
                    tmpImage.setWidth(Integer.parseInt(value));
                    break;
                  case "Height":
                    tmpImage.setHeight(Integer.parseInt(value));
                    break;
                }
              }
              break;
            case "Role":
              if (inNewsComponent) {
                role = streamReader.getAttributeValue(null, "FormalName");
              }
              break;
            case "HeadLine":
              if (inNewsComponent) {
                caption = streamReader.getElementText();
              }
              break;
            case "NewsComponent":
              inNewsComponent = true;
              break;
            case "ContentItem":
              if (inNewsComponent && role.equals("HighDef")) {
                final String href = streamReader.getAttributeValue(null, "Href");
                if (href != null && !href.isEmpty()) {
                  tmpImage = new ImageObject();
                  tmpImage.addDescription(article.getLanguage(), caption);
                  tmpImage.setUrl("http://medialab.afp.com" + href);

                  inContentItem = true;
                }
              }
              break;
          }
        }

        if (streamReader.isEndElement()) {
          switch (streamReader.getLocalName()) {
            case "NewsComponent":
              role = "";
              caption = "";
              inNewsComponent = false;
              break;
            case "ContentItem":
              if (mediaType.equals("Photo")
                  && role.equals("HighDef")
                  && !tmpImage.getUrl().isEmpty()) {
                images.add(tmpImage);
              }

              mediaType = "";
              inContentItem = false;
              break;
          }
        }

        streamReader.next();
      }

    } catch (Exception e) {
      e.printStackTrace();
    }

    return images;
  }

  private static LocalDateTime parseDate(final String dateStr) {
    // Formats known:
    // 20110725T005637Z
    // 20110725T010000+0000
    // 20170207T070104
    LocalDateTime dt;
    try {
      dt = LocalDateTime.parse(dateStr, DateTimeFormatter.ofPattern("yyyyMMdd'T'HHmmss'Z'"));
    } catch (final DateTimeParseException ex) {
      try {
        dt = LocalDateTime.parse(dateStr, DateTimeFormatter.ofPattern("yyyyMMdd'T'HHmmssZ"));
      } catch (final DateTimeParseException ex2) {
        dt = LocalDateTime.parse(dateStr, DateTimeFormatter.ofPattern("yyyyMMdd'T'HHmmss"));
      }
    }
    return dt;
  }
}
