package fr.eurecom.asrael.newscollector;

import fr.eurecom.asrael.commons.utils.Configuration;
import fr.eurecom.asrael.newscollector.agencefrancepresse.AgenceFrancePresse;
import java.io.IOException;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public final class Main {

  public static void main(final String... args) {
    Options options = new Options();

    Option langOption = new Option("l", "lang", true, "lang");
    options.addOption(langOption);

    Option yearOption = new Option("y", "year", true, "year");
    options.addOption(yearOption);

    Option monthOption = new Option("m", "month", true, "month");
    options.addOption(monthOption);

    Option dayOption = new Option("d", "day", true, "day");
    options.addOption(dayOption);

    Option withMediaOption =
        new Option(null, "with-media", false, "Try fetching medias (photos) from afp");
    options.addOption(withMediaOption);

    CommandLineParser parser = new DefaultParser();
    HelpFormatter formatter = new HelpFormatter();
    CommandLine cmd = null;
    try {
      cmd = parser.parse(options, args);
    } catch (ParseException e) {
      System.out.println(e.getMessage());
      formatter.printHelp("clusters-converter", options);
      System.exit(1);
    }

    String lang = cmd.getOptionValue("lang");
    String year = cmd.getOptionValue("year");
    String month = cmd.getOptionValue("month");
    String day = cmd.getOptionValue("day");
    boolean withMedia = cmd.hasOption("with-media");

    try {
      new AgenceFrancePresse(Configuration.load()).run(lang, year, month, day, withMedia);
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }
}
