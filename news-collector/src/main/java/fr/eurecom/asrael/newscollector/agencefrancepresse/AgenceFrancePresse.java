package fr.eurecom.asrael.newscollector.agencefrancepresse;

import fr.eurecom.asrael.commons.datatypes.ImageObject;
import fr.eurecom.asrael.commons.datatypes.NewsArticle;
import fr.eurecom.asrael.commons.utils.Configuration;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AgenceFrancePresse {
  private static final Logger LOGGER = LoggerFactory.getLogger(AgenceFrancePresse.class);
  private final Configuration config;
  private Set<String> forbiddenSubjects;

  public AgenceFrancePresse(Configuration config) {
    this.config = config;

    this.forbiddenSubjects = new HashSet<>();
    for (String forbiddenSubject : config.getForbiddenSubjects()) {
      String[] subjArray = forbiddenSubject.split("-");
      long startSubj = Long.parseLong(subjArray[0]);;
      long endSubj = startSubj;
      if (subjArray.length > 1) {
        endSubj = Long.parseLong(subjArray[1]);
      }
      for (long l = startSubj; l < endSubj; l += 1) {
        this.forbiddenSubjects.add(String.valueOf(l));
      }
    }
  }

  public void run(
      final String lang,
      final String year,
      final String month,
      final String day,
      final boolean withMedia) {
    try {
      // For each language dir (eg. ENG, FRA, GER)
      Files.list(Paths.get(config.getResourcesPath(), config.getDatasetName()))
          .filter(file -> file.toFile().isDirectory())
          .filter(file -> lang == null || file.getFileName().toString().equals(lang))
          .forEach(
              langDir -> {
                try {
                  // For each year dir (eg. 2004)
                  Files.list(langDir)
                      .filter(file -> file.toFile().isDirectory())
                      .filter(file -> year == null || file.getFileName().toString().equals(year))
                      .forEach(
                          yearDir -> {
                            try {
                              // For each month dir (eg. 01)
                              Files.list(yearDir)
                                  .filter(file -> file.toFile().isDirectory())
                                  .filter(
                                      file ->
                                          month == null
                                              || file.getFileName().toString().equals(month))
                                  .forEach(
                                      monthDir -> {
                                        try {
                                          // For each day dir (eg. 31)
                                          Files.list(monthDir)
                                              .filter(file -> file.toFile().isDirectory())
                                              .filter(
                                                  file ->
                                                      day == null
                                                          || file.getFileName()
                                                              .toString()
                                                              .equals(day))
                                              .forEach(
                                                  dayDir ->
                                                      processDir(
                                                          dayDir, monthDir, yearDir, langDir,
                                                          withMedia));
                                        } catch (IOException e) {
                                          AgenceFrancePresse.LOGGER.error(e.toString());
                                        }
                                      });
                            } catch (IOException e) {
                              AgenceFrancePresse.LOGGER.error(e.toString());
                            }
                          });
                } catch (IOException e) {
                  AgenceFrancePresse.LOGGER.error(e.toString());
                }
              });
    } catch (IOException e) {
      AgenceFrancePresse.LOGGER.error(e.toString());
    }
  }

  private void processDir(
      Path dayDir, Path monthDir, Path yearDir, Path langDir, boolean withMedia) {
    final Path turtleFile =
        Paths.get(
            config.getDumpPath(),
            config.getDatasetName(),
            langDir.getFileName().toString(),
            yearDir.getFileName()
                + "_"
                + monthDir.getFileName()
                + "_"
                + dayDir.getFileName()
                + "-"
                + langDir.getFileName()
                + ".news.ttl");

    if (Files.exists(turtleFile)) {
      // TODO: check if --reset flag is set
      AgenceFrancePresse.LOGGER.info("Skipping parsing of " + dayDir.toString());
      return;
    }

    final List<NewsArticle> newsArticles = new ArrayList<>();

    try {
      // For each xml file
      Files.list(dayDir)
          .filter(file -> file.toFile().getName().endsWith(".xml"))
          .sorted()
          .forEach(
              xml -> {
                // Parse the news article
                XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                try (InputStream in = new FileInputStream(xml.toFile())) {
                  NewsArticle article =
                      AgenceFrancePresseMethods.parseArticle(
                          inputFactory.createXMLStreamReader(in));
                  if (article != null) {
                    Set<String> lowerCaseKeywords =
                        article.getKeywords().stream()
                            .map(String::toLowerCase)
                            .collect(Collectors.toSet());
                    if (!Collections.disjoint(lowerCaseKeywords, config.getForbiddenKeywords())) {
                      // Ignore articles with certain keywords
                      AgenceFrancePresse.LOGGER.info(
                          "Skip article with forbidden keyword: "
                              + xml
                              + " (keywords: "
                              + String.join(", ", article.getKeywords())
                              + ")");
                      return;
                    }

                    if (config.getForbiddenGenres().contains(article.getGenre().toLowerCase())) {
                      // Ignore articles with certain genres
                      AgenceFrancePresse.LOGGER.info(
                          "Skip article with forbidden genre: "
                              + xml
                              + " (genre: "
                              + article.getGenre()
                              + ")");
                      return;
                    }

                    Set<String> subjects = new HashSet<>();
                    for (String subject : article.getSubjects()) {
                      long subjectNum = Long.parseLong(subject.substring(subject.lastIndexOf("/") + 1));
                      subjects.add(String.valueOf(subjectNum));
                    }
                    if (!Collections.disjoint(subjects, forbiddenSubjects)) {
                      // Ignore articles with certain subjects
                      AgenceFrancePresse.LOGGER.info(
                          "Skip article with forbidden subject: "
                              + xml
                              + " (subjects: "
                              + String.join(", ", article.getSubjects())
                              + ")");
                      return;
                    }

                    if (withMedia) {
                      List<ImageObject> images =
                          AgenceFrancePresseMethods.getArticleImages(article);
                      images.forEach(article::addAssociatedMedia);
                    }

                    newsArticles.add(article);
                  }
                } catch (final IOException | XMLStreamException e) {
                  AgenceFrancePresse.LOGGER.error(xml + ": " + e.toString());
                }
              });
    } catch (IOException e) {
      AgenceFrancePresse.LOGGER.error(e.toString());
    }

    // Create a new RDF model with content from parsed articles
    final Model finalModel = ModelFactory.createDefaultModel();
    for (final NewsArticle news : newsArticles) {
      finalModel.add(news.getRDFModel());
    }

    // Make sure that the dump directory exists
    try {
      Files.createDirectories(
          Paths.get(
              config.getDumpPath(), config.getDatasetName(), langDir.getFileName().toString()));
    } catch (IOException e) {
      AgenceFrancePresse.LOGGER.error(e.toString());
    }

    // Write the model to a turtle file
    try (final OutputStream outRdf =
        Files.newOutputStream(turtleFile, StandardOpenOption.CREATE, StandardOpenOption.WRITE)) {
      RDFDataMgr.write(outRdf, finalModel, RDFFormat.TURTLE_PRETTY);
    } catch (final IOException ex) {
      AgenceFrancePresse.LOGGER.error("Issue with the filesystem", ex);
    }
  }
}
