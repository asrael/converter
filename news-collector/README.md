# NewsCollector

## Compilation

```
mvn -U clean package
```

## Usage

```
java -Dlogfile.name=asrael-newscollector.log -Dlogfile.append=true -jar target/NewsCollector-1.0-SNAPSHOT.jar
```
