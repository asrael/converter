#!/usr/bin/env bash

# Convert a formatted date string to an unix timestamp
# $1 YYYY-MM-DD date string
date_to_epoch() {
  local date_ymd
  date_ymd="${1}"
  if [[ "$OSTYPE" == "darwin"* ]]; then
    echo $(date -j -f "%F" "${date_ymd}" +"%s")
  else
    echo $(date -d "${date_ymd}" +%s)
  fi
}

# Get the last day of the month
# $1 year
# $2 month
last_day_of_month() {
  local date_y="${1}"
  local date_m="${2}"
  if [[ "$OSTYPE" == "darwin"* ]]; then
    echo $(date -v${date_y}y -v${date_m}m -v+1m -v1d -v-1d +"%d")
  else
    echo $(date -d "${date_y}/${date_m}/1 + 1 month - 1 day" +%d)
  fi
}

# Return the absolute path to a file
# $1 path
realpath() {
  echo $(cd $(dirname $1); pwd)/$(basename $1);
}

cd "$(dirname "${BASH_SOURCE[0]}")"
CWD="${PWD}"

data_dir=$(realpath "../data")
mkdir -p "${data_dir}/resource/source-extractor/xml"
mkdir -p "${data_dir}/resource/source-extractor/output"

### Step 1

echo "Removing previous XML files from ${data_dir}/resource/source-extractor/xml"

find "${data_dir}/resource/source-extractor/xml" -type f -name "*.xml" -delete
find "${data_dir}/resource/source-extractor" -type f -name "out.pkl" -delete
find "${data_dir}/resource/source-extractor" -type f -name "out.json" -delete

echo "Copying XML files..."

target_date="${ASRAEL_DATE-$(date +'%Y-%m-%d')}"
date_path=$(echo "${target_date}" | sed 's#-#\/#g')

echo "Date: ${target_date}"

xml_count=0

lang_dir="${data_dir}/resource/agencefrancepresse/FRA"
if [[ -d $lang_dir ]]; then
  lang_dir=${lang_dir%/}
  echo "Copying from ${lang_dir}/${date_path}"
  for f in "${lang_dir}/${date_path}"/*.xml; do
    cp "${f}" "${data_dir}/resource/source-extractor/xml/"
    xml_count=$(($xml_count+1))
  done
fi

echo "Copied ${xml_count} XML files into ${data_dir}/resource/source-extractor/xml/"

### Step 2

echo "Running source extractor..."

cd "${CWD}/../../limsi-extractor" || exit 1
node index.js -i "${data_dir}/resource/source-extractor/xml/" -o "${data_dir}/resource/source-extractor/output/"
