#!/usr/bin/env python
# coding: utf-8

'''
Convert a schema classification pkl file into json file
'''

import sys
import os
import _pickle as pickle
import json

def convert_dict_to_json(file_path):
  with open(file_path, 'rb') as fpkl, open('%s.json' % file_path, 'w') as fjson:
    data = pickle.load(fpkl)
    output = dict()
    
    for schema_name, items in data.items():
      if schema_name != 'text':
        schema_id = schema_name.split(' - ')[0].strip()
        for xml_filename, has_class in items.items():
          if has_class == 1:
            if xml_filename not in output:
              output[xml_filename] = list()
            output[xml_filename].append(schema_id)

    json.dump(output, fjson, indent=2)

def main():
  if sys.argv[1] and os.path.isfile(sys.argv[1]):
    file_path = sys.argv[1]
    print("Processing %s ..." % file_path)
    convert_dict_to_json(file_path)
  else:
    print("Usage: %s abs_file_path" % (__file__))

if __name__ == '__main__':
  main()
