#!/usr/bin/env bash

#Cron:
#- Remove files from `news-kb/data/resource/clustering/xml`
#- Remove files from `news-kb/data/resource/clustering/txt`
#- Remove files from `news-kb/data/resource/clustering/json`
#- Remove files from `news-kb/data/resource/clustering/lst`
#- Take all XML files from 30 last days since CURRENT_DATE, put them in `news-kb/data/resource/clustering/xml`
#- Execute `python src/extract_corpus_from_xml.py news-kb/data/resource/clustering/xml news-kb/data/resource/clustering/txt`
#- Execute `python src/StanfordNLPCustom/corpusParser_parallel.py news-kb/data/resource/clustering/txt news-kb/data/resource/clustering/json`
#- Execute `find "$(cd news-kb/data/resource/clustering/xml; pwd)" -name "*.xml" > news-kb/data/resource/clustering/lst/xml.lst`
#- Execute `find "$(cd news-kb/data/resource/clustering/json; pwd)" -name "*.json" > news-kb/data/resource/clustering/lst/json.lst`
#- Execute `./afp_clustering_json_parallel_grid.sh news-kb/data/resource/clustering/lst`
#  - Need to add ability to pass the path to lst (and maybe other paths that are required?) to the .sh script
#- Parse results from .clust XML file and convert its into Turtle

# Convert a formatted date string to an unix timestamp
# $1 YYYY-MM-DD date string
date_to_epoch() {
  local date_ymd
  date_ymd="${1}"
  if [[ "$OSTYPE" == "darwin"* ]]; then
    echo $(date -j -f "%F" "${date_ymd}" +"%s")
  else
    echo $(date -d "${date_ymd}" +%s)
  fi
}

# Get tomorrow date from a specific date, formatted as YYYY-MM-DD
tomorrow() {
  local date_ymd
  date_ymd="${1}"
  if [[ "$OSTYPE" == "darwin"* ]]; then
    echo $(date -j -f "%Y-%m-%d" -v+1d "${date_ymd}" +"%Y-%m-%d")
  else
    echo $(date -I -d "${date_ymd} + 1 day")
  fi
}

# Convert an unix timestamp to a formatted date (YYYY-MM-DD)
epoch_to_date() {
  local timestamp
  timestamp="${1}"
  if [[ "$OSTYPE" == "darwin"* ]]; then
    echo $(date -r "${timestamp}" +"%Y-%m-%d")
  else
    echo $(date -d "@${timestamp}" +"%Y-%m-%d")
  fi
}

# Get the last day of the month
# $1 year
# $2 month
last_day_of_month() {
  local date_y="${1}"
  local date_m="${2}"
  if [[ "$OSTYPE" == "darwin"* ]]; then
    echo $(date -v${date_y}y -v${date_m}m -v+1m -v1d -v-1d +"%d")
  else
    echo $(date -d "${date_y}/${date_m}/1 + 1 month - 1 day" +%d)
  fi
}

# Return the absolute path to a file
# $1 path
realpath() {
  echo "$(cd $(dirname "${1}"); pwd)/$(basename "${1}")";
}

# Clusterize
# $1 lang_dir (eg. "/path/to/data/ENG")
# $2 data_dir (eg. "../data/resource/clustering")
# $4 min_date (eg. "2020-06-13")
# $4 max_date (eg. "2020-06-27")
# $4 language (for CoreNLP, eg. "english", "french")
clusterize() {
  local lang_dir="${1}"
  local data_dir="${2}"
  local min_date="${3}"
  local max_date="${4}"
  local language="${5}"

  echo "clusterize (lang_dir=${lang_dir}, data_dir=${data_dir}, min_date=${min_date}, max_date=${max_date} language=${language})"

  local xml_dir="${data_dir}/xml"
  local txt_dir="${data_dir}/txt"
  local json_dir="${data_dir}/json"
  local lst_dir="${data_dir}/lst/corpus"
  local out_dir="${data_dir}/out"
  local xml_count=0

  cd "${CWD}" || return

  if [[ ! -d "${data_dir}" ]]; then
    echo "${data_dir}: Directory does not exist."
    return 1
  fi

  ### Step 0

  echo "Cleaning up previous clusterization..."

  mkdir -p "${xml_dir}"
  mkdir -p "${txt_dir}"
  mkdir -p "${json_dir}"
  mkdir -p "${lst_dir}/json"
  mkdir -p "${lst_dir}/txt"
  mkdir -p "${lst_dir}/xml"
  mkdir -p "${out_dir}"

  find "${xml_dir}" -type f -name "*.xml" -delete
  find "${lst_dir}" -type f -name "*.lst" -delete
  rm -rf "${out_dir}"

  # Navigate to afp_clustering module
  cd "../../afp_clustering" || exit 1

  #git pull --rebase || exit 1

  ### Step 1

  echo "Copying XML files..."

  # Copy XML files
  copy_end_date=$(tomorrow "${max_date}")
  copy_cur_date="${min_date}"
  while [ "${copy_cur_date}" != "${copy_end_date}" ]; do
    echo "${copy_cur_date}"
    copy_file_date=$(echo "${copy_cur_date}" | sed -e 's/-//g')
    for f in $(find "${lang_dir}" -type f -name "afp.com-${copy_file_date}T*.xml"); do
      cp "${f}" "${xml_dir}/"
      xml_count=$(($xml_count+1))
    done

    copy_cur_date=$(tomorrow "${copy_cur_date}")
  done

  echo "Copied ${xml_count} XML files into ${xml_dir}"

  ### Step 2

  echo "Preprocessing XML files... (filtering undeeded documents and grouping them by ITPC codes)"
  docker run --rm -u `id -u` -v "${data_dir}:/data" asrael/afp_clustering sh -c "cd /home/docker/app/src && conda run -n asrael-clustering python preprocess_xml.py /data/xml"

  # Checking if we still have some documents left...
  post_filter_xml_count=$(find "${xml_dir}" -type f -name '*.xml' -exec basename "{}" \; | sort -u | wc -l | xargs)
  echo "Removed $(($xml_count-$post_filter_xml_count)) files (before: $xml_count, after: ${post_filter_xml_count})"
  if [ "${post_filter_xml_count}" -le "0" ]; then
    echo "No documents were found, exiting..."
    return 0
  fi

  ### Step 3

  echo "Extracting corpus from XML..."
  docker run --rm -u `id -u` -v "${data_dir}:/data" asrael/afp_clustering sh -c "cd /home/docker/app/src && conda run -n asrael-clustering python extract_corpus_from_xml.py /data/xml /data/txt"

  # Delete previous txt listing
  find "${lst_dir}/txt" -type f -name "*.lst" -delete

  # Generate list of txt to parse into JSON in Step 3
  find "${xml_dir}" -type f -name '*.xml' | while read i; do
    xml_filename="$(basename "$i")"
    echo "/data/txt/${xml_filename%.*}.txt" >> "${lst_dir}/txt/corpus.lst"
  done

  ### Step 4

  echo "Parsing corpus..."
  docker run --rm -u `id -u` -v "${data_dir}:/data" asrael/afp_clustering sh -c "cd /home/docker/app/src && conda run -n asrael-clustering python StanfordNLPCustom/corpusParser_parallel.py --language \"${language}\" /data/lst/corpus/txt/corpus.lst /data/json"

  # For each IPTC directory...
  find "${xml_dir}" -mindepth 1 -type d | while read dir; do
    echo "Working with ${dir}"
    iptc_group=$(basename "${dir}")

    ### Step 5

    echo "Generating listing files"
    docker run --rm -u `id -u` -v "${data_dir}:/data" asrael/afp_clustering sh -c "cd /home/docker/app/script && ./generate_listing.sh \"/data/xml/${iptc_group}\" /data"

    if [ -f "${lst_dir}/xml/corpus.lst" ] && [ -f "${lst_dir}/json/corpus.lst" ]; then
      ### Step 6

      if [ "${language}" = "french" ]; then
        LNG="fr"
      else
        LNG="en"
      fi
      echo "Executing afp_clustering_json_parallel_grid"
      docker run --rm -u `id -u` -v "${data_dir}:/data" asrael/afp_clustering sh -c "cd /home/docker/app && LST_DIR=/data/lst/corpus OUT_BASE=/data/out LNG=\"${LNG}\" conda run -n asrael-clustering ./afp_clustering_json_parallel_grid.sh"

      ### Step 7

      echo "Converting clusters to RDF"
      cd "${CWD}/.." || return
      mvn -q exec:java -pl clusters-converter -Dexec.args="--group ${iptc_group}"
    else
      echo "No listings found, skip clustering"
    fi
  done
}

cd "$(dirname "${BASH_SOURCE[0]}")" || exit
CWD="${PWD}"
echo "CWD: ${CWD}"

target_date="${ASRAEL_DATE-$(date +'%Y-%m-%d')}"
target_epoch=$(date_to_epoch "${target_date}")
days=${DAYS:-14}
seconds=$(($days*24*60*60))
one_day_seconds=$((1*24*60*60))
min_epoch=$(($target_epoch - $seconds))
max_epoch=$(($target_epoch + $one_day_seconds))
min_date=$(epoch_to_date "${min_epoch}")
max_date=$(epoch_to_date "${max_epoch}")

# Mapping between agencefrancepresse directory name (eg. "ENG") and CoreNLP language name (eg. "english")
declare -A languages=(["ENG"]="english" ["FRA"]="french")

echo "Date: ${target_date}"
echo "Days: ${days}"

xml_path=$(realpath "../data/resource/agencefrancepresse")
clustering_path=$(realpath "../data/resource/clustering")
for lang_dir in "${xml_path}/"*; do
  if [[ -d "${lang_dir}" ]]; then
    echo "directory: ${lang_dir}"
    lang=$(basename "${lang_dir}")
    if [[ -z "${languages["${lang}"]:-}" ]]; then
      echo "WARN: Language directory '${lang}' not found in mapping (${!languages[@]}). Defaulting to english"
      lang="english"
    fi
    clusterize "${lang_dir}" "${clustering_path}" "${min_date}" "${max_date}" "${languages["${lang}"]}" || exit
  else
    echo "${lang_dir}: Not a directory. It has been ignored."
  fi
done
