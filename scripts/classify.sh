#!/usr/bin/env bash

#Cron:
#- Remove xml files from `news-kb/data/resource/classification/xml`
#- Remove out.pkl and out.json from `news-kb/data/resource/classification`
#- Take all XML files from 30 last days since CURRENT_DATE, put them in `news-kb/data/resource/classification/xml`
#- Run `schema_classification/03-apply_schema_classifiers.py`
#- Run `news-kb/scripts/schema_classification_pkl_to_json.py schema_classification/data/out.pkl news-kb/data/resource/classification/
#- Run classification-converter to convert the output to RDF

# Convert a formatted date string to an unix timestamp
# $1 YYYY-MM-DD date string
date_to_epoch() {
  local date_ymd
  date_ymd="${1}"
  if [[ "$OSTYPE" == "darwin"* ]]; then
    echo $(date -j -f "%F" "${date_ymd}" +"%s")
  else
    echo $(date -d "${date_ymd}" +%s)
  fi
}

# Get the last day of the month
# $1 year
# $2 month
last_day_of_month() {
  local date_y="${1}"
  local date_m="${2}"
  if [[ "$OSTYPE" == "darwin"* ]]; then
    echo $(date -v${date_y}y -v${date_m}m -v+1m -v1d -v-1d +"%d")
  else
    echo $(date -d "${date_y}/${date_m}/1 + 1 month - 1 day" +%d)
  fi
}

# Return the absolute path to a file
# $1 path
realpath() {
  echo $(cd $(dirname $1); pwd)/$(basename $1);
}

cd "$(dirname "${BASH_SOURCE[0]}")"
CWD="${PWD}"

data_dir=$(realpath "../data")
mkdir -p "${data_dir}/resource/classification/xml"

### Step 1

if [ -z "$SKIP_STEP_1" ]; then
  echo "Removing previous XML files from ${data_dir}/resource/classification/xml"

  find "${data_dir}/resource/classification/xml" -type f -name "*.xml" -delete
  find "${data_dir}/resource/classification" -type f -name "out.pkl" -delete
  find "${data_dir}/resource/classification" -type f -name "out.json" -delete

  echo "Copying XML files..."

  target_date="${ASRAEL_DATE-$(date +'%Y-%m-%d')}"
  date_path=$(echo "${target_date}" | sed 's#-#\/#g')

  echo "Date: ${target_date}"

  xml_count=0

  for lang_dir in "${data_dir}/resource/agencefrancepresse/"*; do
    if [[ -d $lang_dir ]]; then
      lang_dir=${lang_dir%/}
      echo "Copying from ${lang_dir}/${date_path}"
      for f in "${lang_dir}/${date_path}"/*.xml; do
        cp "${f}" "${data_dir}/resource/classification/xml/"
        xml_count=$(($xml_count+1))
      done
    fi
  done

  echo "Copied ${xml_count} XML files into ${data_dir}/resource/classification/xml/"
else
  echo "Skipped Step 1"
fi

### Step 2

if [ -z "$SKIP_STEP_2" ]; then
  # echo "Running classifier..."
  # docker run -u `id -u` --rm -v "${data_dir}:/data" asrael/zeroshot-schema-classification sh -c "./start.sh"

  echo "Removing previous XML files from remote server"
  ssh semantic@d2k.eurecom.fr 'find "/home/semantic/asrael/zeroshot-schema-classification/data/resource/agencefrancepresse" -type f -name "*.xml" -delete' || exit 1

  echo "Copying XML files to remote server"
  rsync -avzq -e 'ssh' "${data_dir}/resource/classification/xml/" semantic@d2k.eurecom.fr:"/home/semantic/asrael/zeroshot-schema-classification/data/resource/agencefrancepresse" || exit 1

  echo "Running classifier on remote server"
  ssh semantic@d2k.eurecom.fr 'cd /home/semantic/asrael/zeroshot-schema-classification && git pull --rebase && CUDA_VISIBLE_DEVICES=1 conda run -n ehrhart-asrael-transformers ./start.sh' || exit 1

  echo "Downloading results from remote server"
  rsync -avz -e 'ssh' semantic@d2k.eurecom.fr:"/home/semantic/asrael/zeroshot-schema-classification/data/resource/classification/df_schemas_predictions.json" "${data_dir}/resource/classification/df_schemas_predictions.json"
else
  echo "Skipped Step 2"
fi

### Step 3

if [ -z "$SKIP_STEP_4" ]; then
  echo "Converting classification to RDF..."

  cd "${CWD}/.." || exit 1
  mvn -q exec:java -pl classification-converter
else
  echo "Skipped Step 3"
fi
