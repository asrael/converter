#!/usr/bin/env bash

# Convert a formatted date string to an unix timestamp
# $1 YYYY-MM-DD date string
date_to_epoch() {
  local date_ymd
  date_ymd="${1}"
  if [[ "$OSTYPE" == "darwin"* ]]; then
    echo $(date -j -f "%F" "${date_ymd}" +"%s")
  else
    echo $(date -d "${date_ymd}" +%s)
  fi
}

# Get the last day of the month
# $1 year
# $2 month
last_day_of_month() {
  local date_y="${1}"
  local date_m="${2}"
  if [[ "$OSTYPE" == "darwin"* ]]; then
    echo $(date -v${date_y}y -v${date_m}m -v+1m -v1d -v-1d +"%d")
  else
    echo $(date -d "${date_y}/${date_m}/1 + 1 month - 1 day" +%d)
  fi
}

# Return the absolute path to a file
# $1 path
realpath() {
  echo $(cd $(dirname $1); pwd)/$(basename $1);
}

cd "$(dirname "${BASH_SOURCE[0]}")" || exit
CWD="${PWD}"
echo "CWD: ${CWD}"

data_dir=$(realpath "../data")

# Navigate to afp2wiki module
cd "../../afp2wiki" || exit 1

### Step 0

git pull --rebase || exit 1
mkdir -p ${PWD}/data/inputs/AFP/ || exit 1

### Step 1

echo "Removing previous files..."
find "$PWD/data/inputs/AFP" -type f -name "*.xml" -delete
find "$PWD/data/AFP/cleaned" -type f -name "*.pkl" -delete
find "$PWD/data/preprocessed-file/matches_iptc/" -type f -name "*.pkl" -delete
find "$PWD/data/preprocessed-file/matches_iptc/" -type f -name "*.json" -delete
find "$PWD/data/preprocessed-file/" -type f -name "*.tsv" -delete
find "${data_dir}/resource/afp2wiki/" -type f -name "*.json" -delete

echo "Copying XML files..."

target_date="${ASRAEL_DATE-$(date +'%Y-%m-%d')}"
date_path=$(echo "${target_date}" | sed 's#-#\/#g')

echo "Date: ${target_date}"

xml_count=0

for lang_dir in "${data_dir}/resource/agencefrancepresse/"*; do
  if [[ -d $lang_dir ]]; then
    lang_dir=${lang_dir%/}
    echo "Copying from ${lang_dir}/${date_path}"
    for f in "${lang_dir}/${date_path}"/*.xml; do
      cp "${f}" "${PWD}/data/inputs/AFP/"
      xml_count=$(($xml_count+1))
    done
  fi
done

echo "Copied ${xml_count} XML files into ${PWD}/data/inputs/AFP/"

if [[ "${xml_count}" -eq 0 ]]; then
  echo "Nothing to do, exiting..."
  exit 0
fi

### Step 2

echo "Preprocessing AFP dataset..."
docker run --rm -u `id -u` -v "$PWD/data":/home/app/data asrael/afp2wiki python -m afp2wiki.preprocess_afp

# ### Step 3

echo "Creating text files with 3/5/all sentences from AFP dataset..."
docker run --rm -u `id -u` -v "$PWD/data":/home/app/data asrael/afp2wiki python -m afp2wiki.create_afp_with_3_5_all_sentences

### Step 4

echo "Matching dump properties with AFP dataset..."
docker run --rm -u `id -u` -v "$PWD/data":/home/app/data asrael/afp2wiki python -m afp2wiki.match_dump_properties_with_AFP --threshold 0.04 --sentences 3

### Step 5

echo "Generating matches between AFP dataset and Wikidata instances..."
docker run --rm -u `id -u` -v "$PWD/data":/home/app/data asrael/afp2wiki python -m afp2wiki.Matches --threshold 0.04 --sentences 3

### Step 6

echo "Copying matches..."
mkdir -p "${data_dir}/resource/afp2wiki/"
find "$PWD/data/preprocessed-file/matches_iptc/" -type f -name "matches_afp2wiki.wos.flexloc.*.json" -exec cp {} "${data_dir}/resource/afp2wiki/" \;

### Step 7

echo "Converting matches to RDF..."
cd "${CWD}/.." || exit 1
mvn -q exec:java -pl afp2wiki-converter
