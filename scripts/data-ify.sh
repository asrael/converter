#!/usr/bin/env bash

# Convert a formatted date string to an unix timestamp
# $1 YYYY-MM-DD date string
date_to_epoch() {
  local date_ymd
  date_ymd="${1}"
  if [[ "$OSTYPE" == "darwin"* ]]; then
    echo $(date -j -f "%F" "${date_ymd}" +"%s")
  else
    echo $(date -d "${date_ymd}" +%s)
  fi
}

# Get the last day of the month
# $1 year
# $2 month
last_day_of_month() {
  local date_y="${1}"
  local date_m="${2}"
  if [[ "$OSTYPE" == "darwin"* ]]; then
    echo $(date -v${date_y}y -v${date_m}m -v+1m -v1d -v-1d +"%d")
  else
    echo $(date -d "${date_y}/${date_m}/1 + 1 month - 1 day" +%d)
  fi
}

# Return the absolute path to a file
# $1 path
realpath() {
  echo $(cd $(dirname $1); pwd)/$(basename $1);
}

cd "$(dirname "${BASH_SOURCE[0]}")" || exit
CWD="${PWD}"
echo "CWD: ${CWD}"

data_dir=$(realpath "../data")

# Navigate to news-annotations module
cd "../../news-annotations" || exit 1

### Step 0

git pull --rebase || exit 1

echo "Removing previous files..."

mkdir -p "${PWD}/files/news/AFP_reference/"
find "${PWD}/files/news/AFP_reference/" -type f -name "*.xml" -delete

mkdir -p "${data_dir}/resource/annotations"
find "${data_dir}/resource/annotations" -type f -name "*.ann" -delete

### Step 1

echo "Copying XML files..."

target_date="${ASRAEL_DATE-$(date +'%Y-%m-%d')}"
date_path=$(echo "${target_date}" | sed 's#-#\/#g')

echo "Date: ${target_date}"

xml_count=0

for lang_dir in "${data_dir}/resource/agencefrancepresse/"*; do
  if [[ -d $lang_dir ]]; then
    lang_dir=${lang_dir%/}
    echo "Copying from ${lang_dir}/${date_path}"
    for f in "${lang_dir}/${date_path}"/*.xml; do
      cp "${f}" "${PWD}/files/news/AFP_reference/"
      xml_count=$(($xml_count+1))
    done
  fi
done

echo "Copied ${xml_count} XML files into ${PWD}/files/news/AFP_reference/"

if [[ "${xml_count}" -eq 0 ]]; then
  echo "Nothing to do, exiting..."
  exit 0
fi

### Step 2

echo "Running data-ification process..."
docker run --rm -v "$PWD/files":/home/app/files asrael/news-annotations bash -c "(cd src/ && python preprocess.py) || exit 1" || exit 1
docker run --rm -v "$PWD/files":/home/app/files asrael/news-annotations bash -c "(cd src/ && python extract_dates_save_index.py) || exit 1" || exit 1
docker run --rm -v "$PWD/files":/home/app/files asrael/news-annotations bash -c "(cd src/ && python generate_list.py --date \"${target_date}\") || exit 1" || exit 1
docker run --rm -v "$PWD/files":/home/app/files asrael/news-annotations bash -c "(cd src/ && python annotate_article.py brat) || exit 1" || exit 1

### Step 3

echo "Copy new annotations for RDF conversion..."
find "${PWD}/files/news/annotations" -type f -name "*.ann" -exec cp {} "${data_dir}/resource/annotations/" \;

echo "Convert annotations to RDF..."
cd "${CWD}/.." || return
mvn -q exec:java -pl brat-annotator || exit 1
