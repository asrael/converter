# converter

Tools for building the ASRAEL news KB including news collector (converting AFP XML press releases in RDF following the rNews ontology), named entity annotator (using ADEL), source annotation (using the LIMSI source extractor).

## Requirements

* Java 8
* Maven 3

## Maven Compilation

To compile and install the modules, use the following command:

```bash
mvn clean install
```

## Configuration

Before running any of the modules, it is important to set up the configuration variables.

Copy the file [props/default.yml](props/default.yml) into `props/config.yml` and open it to set up the properties:

| Property | Module | Description | Default Value |
|---|---|---|---|
| `resourcesPath` | all | Path to the resources directory. | `./data/resource` |
| `dumpPath` | all | Path to the dump directory. | `./data/dump` |
| `annotatorPath` | _brat-annotator_ | Path to the src folder of the news-annotations project. | `/path/to/news-annotations/src` |
| `annotatorFile` | _brat-annotator_ | Name of the script file used to annotate articles. | `annotate_article.py` |
| `datasetName` | _news-collector_ | Name of the dataset directory. Must exist and be relative to `resourcesPath`. Also used for generating the output in `dumpPath`. | `agencefrancepresse` |
| `forbiddenKeywords` | _news-collector_ | List of keywords used to ignore certain news. | see [default.yml](props/default.yml) |
| `forbiddenGenres` | _news-collector_ | List of keywords used to ignore certain news. | see [default.yml](props/default.yml) |
| `classificationJsonPath` | _classification-converter_ | Path to the JSON output file generated during classification | `./data/resource/classification/out.pkl.json` |
| `classificationXmlPath` | _classification-converter_ | Path to the XML directory used during classification | `./data/resource/classification/xml` |
| `clustersPath` | _clusters-converter_ | Path to the output from the clustering program. | `./data/resource/clustering/out` |

## Run

### News Collector

```bash
mvn exec:java -pl news-collector
```

Optional parameters can be passed using `-Dexec.args`, for example :

```bash
mvn exec:java -pl news-collector -Dexec.args="--year 2020 --month 05 --day 21"
```

List of parameters:

| Parameter | Description | Default Value |
|---|---|---|
| `--year` (`-y`) | Year to convert | |
| `--month` (`-m`) | Month to convert | |
| `--day` (`-d`) | Day to convert | |
| `--with-media` | Try fetching media (photos) from afp | false |

### Adel Annotator

```bash
mvn exec:java -pl adel-annotator
```

### Brat Annotator

```bash
mvn exec:java -pl brat-annotator
```

### Clusters Converter

```bash
mvn exec:java -pl clusters-converter -Dexec.args="--group iptc_group"
```

### Classification Converter

```bash
mvn exec:java -pl classification-converter
```

## Package for distribution

```bash
mvn clean package
```
