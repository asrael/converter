package fr.eurecom.asrael.clustersconverter;

import fr.eurecom.asrael.commons.utils.Configuration;
import fr.eurecom.asrael.commons.utils.FileUtils;
import fr.eurecom.asrael.commons.utils.UUIDGenerator;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.vocabulary.RDF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ClustersConverter {
  private static final Logger LOGGER = LoggerFactory.getLogger(ClustersConverter.class);
  private final Configuration config;

  public ClustersConverter(Configuration config) {
    this.config = config;
  }

  public void run(String iptcGroup) {
    try {
      Files.list(Paths.get(config.getClustersPath()))
          .filter(file -> file.toFile().isDirectory())
          .forEach(
              clusteringLangDir -> {
                try {
                  Files.list(clusteringLangDir)
                      .filter(file -> file.toFile().isDirectory())
                      .forEach(
                          clusteringDir -> {
                            processClusteringDir(Paths.get(clusteringDir.toString(), "corpus"), iptcGroup);
                          });
                } catch (IOException e) {
                  e.printStackTrace();
                }
              });
    } catch (IOException e) {
      LOGGER.error(e.toString());
    }
  }

  private void processClusteringDir(Path clusteringDir, String iptcGroup) {
    try {
      Files.list(clusteringDir)
          .filter(file -> file.toFile().getName().endsWith(".clust"))
          .forEach(
              file -> {
                // Get file creation date for cluster id and output file name generation
                BasicFileAttributes attr = null;
                try {
                  attr = Files.readAttributes(file, BasicFileAttributes.class);
                } catch (IOException e) {
                  e.printStackTrace();
                }
                FileTime fileTime = attr.lastModifiedTime();
                DateFormat df = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'");
                final String clusteringDate = df.format(fileTime.toMillis());

                final Model model = ModelFactory.createDefaultModel();

                // Add asrael namespace to the final model
                model.setNsPrefix("asrael", "http://asrael.eurecom.fr/asrael#");
                model.setNsPrefix("schema", "http://schema.org/");

                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                try {
                  DocumentBuilder builder = factory.newDocumentBuilder();
                  Document document = builder.parse(file.toFile());
                  NodeList nList = document.getElementsByTagName("cluster");
                  for (int i = 0; i < nList.getLength(); i++) {
                    Node node = nList.item(i);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                      Element element = (Element) node;
                      final String firstEventIdentifier =
                          element.getAttribute("first_evt_identifier");

                      final Resource clusterEntity =
                          ResourceFactory.createResource(
                              "http://asrael.eurecom.fr/cluster/"
                                  + UUIDGenerator.generateUUID(
                                      firstEventIdentifier + "|" + iptcGroup));

                      // RDF Type
                      model.add(
                          clusterEntity,
                          RDF.type,
                          ResourceFactory.createResource("http://schema.org/CreativeWork"));

                      // Date
                      model.add(
                          clusterEntity,
                          ResourceFactory.createProperty("http://schema.org/dateCreated"),
                          ResourceFactory.createTypedLiteral(
                              fileTime.toString(), XSDDatatype.XSDdateTime));

                      // Category (iptc group)
                      model.add(
                          clusterEntity,
                          ResourceFactory.createProperty("http://schema.org/category"),
                          ResourceFactory.createPlainLiteral(iptcGroup));

                      // Lead
                      model.add(
                          clusterEntity,
                          ResourceFactory.createProperty("http://asrael.eurecom.fr/asrael#lead"),
                          ResourceFactory.createResource(
                              "http://asrael.eurecom.fr/news/"
                                  + UUIDGenerator.generateUUID(firstEventIdentifier)));

                      NodeList nDocList = element.getElementsByTagName("doc");
                      for (int j = 0; j < nDocList.getLength(); j++) {
                        Node nDoc = nDocList.item(j);
                        if (nDoc.getNodeType() == Node.ELEMENT_NODE) {
                          Element eDoc = (Element) nDoc;
                          final String docIdentifier = eDoc.getAttribute("identifier");

                          // Is Part Of
                          model.add(
                              ResourceFactory.createResource(
                                  "http://asrael.eurecom.fr/news/"
                                      + UUIDGenerator.generateUUID(docIdentifier)),
                              ResourceFactory.createProperty("http://schema.org/isPartOf"),
                              clusterEntity);
                        }
                      }
                    }
                  }
                } catch (ParserConfigurationException | SAXException | IOException e) {
                  e.printStackTrace();
                }

                // Write output
                final String rdfFile = iptcGroup + "_" + clusteringDate + ".clusters.ttl";
                final Path outputDir = Paths.get(config.getDumpPath(), "clustering");
                Path outFile = FileUtils.getUniquePath(Paths.get(outputDir.toString(), rdfFile));

                // Make sure that the dump directory exists
                try {
                  Files.createDirectories(outputDir);
                } catch (IOException e) {
                  LOGGER.error(e.toString());
                }

                LOGGER.info("Writing annotations to file {}", outFile);
                try (final OutputStream outRdf =
                    Files.newOutputStream(
                        outFile, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING)) {
                  RDFDataMgr.write(outRdf, model, RDFFormat.TURTLE_PRETTY);
                } catch (final IOException e) {
                  LOGGER.error("Issue to write annotated file", e);
                }
              });
    } catch (IOException e) {
      LOGGER.error(e.toString());
    }
  }
}
