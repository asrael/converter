package fr.eurecom.asrael.clustersconverter;

import fr.eurecom.asrael.commons.utils.Configuration;
import java.io.IOException;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class Main {

  public static void main(final String... args) {
    Options options = new Options();

    Option iptcGroupOption = new Option("g", "group", true, "iptc group");
    iptcGroupOption.setRequired(true);
    options.addOption(iptcGroupOption);

    CommandLineParser parser = new DefaultParser();
    HelpFormatter formatter = new HelpFormatter();
    CommandLine cmd = null;
    try {
      cmd = parser.parse(options, args);
    } catch (ParseException e) {
      System.out.println(e.getMessage());
      formatter.printHelp("clusters-converter", options);
      System.exit(1);
    }

    String iptcGroup = cmd.getOptionValue("group");

    try {
      new ClustersConverter(Configuration.load()).run(iptcGroup);
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }
}
