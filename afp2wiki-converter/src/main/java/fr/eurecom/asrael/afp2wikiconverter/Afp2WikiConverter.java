package fr.eurecom.asrael.afp2wikiconverter;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import fr.eurecom.asrael.commons.utils.Configuration;
import fr.eurecom.asrael.commons.utils.FileUtils;
import fr.eurecom.asrael.commons.utils.UUIDGenerator;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Afp2WikiConverter {
  private static final Logger LOGGER = LoggerFactory.getLogger(Afp2WikiConverter.class);
  private final Configuration config;

  public Afp2WikiConverter(Configuration config) {
    this.config = config;
  }

  public void run() {
    DateFormat df = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'");
    final String rdfFile = df.format(new Date()) + ".afp2wiki.ttl";
    final Path outputDir = Paths.get(config.getDumpPath(), "afp2wiki");
    Path outFile = FileUtils.getUniquePath(Paths.get(outputDir.toString(), rdfFile));

    try {
      Files.list(Paths.get(config.getAfp2WikiMatchesPath()))
          .filter(file -> file.toFile().isFile())
          .forEach(matchesFile -> processMatchesFile(matchesFile, outputDir, outFile));
    } catch (IOException e) {
      LOGGER.error(e.toString());
    }
  }

  private void processMatchesFile(Path matchesFile, Path outputDir, Path outFile) {
    try {
      final Model model = ModelFactory.createDefaultModel();

      // Add asrael namespace to the final model
      model.setNsPrefix("asrael", "http://asrael.eurecom.fr/asrael#");
      model.setNsPrefix("schema", "http://schema.org/");
      model.setNsPrefix("owl", "http://www.w3.org/2002/07/owl#");
      model.setNsPrefix("wd", "http://www.wikidata.org/entity/");

      JsonParser parser = new JsonParser();
      JsonObject rootObj = parser.parse(new FileReader(matchesFile.toFile())).getAsJsonObject();
      for (String key : rootObj.keySet()) {
        JsonObject matchObj = rootObj.getAsJsonObject(key);
        String identifier = matchObj.get("identifier").getAsString();
        String[] l_wiki_id =
            new Gson().fromJson(matchObj.getAsJsonArray("l_wiki_id"), String[].class);

        final Resource newsEntity =
            ResourceFactory.createResource(
                "http://asrael.eurecom.fr/news/" + UUIDGenerator.generateUUID(identifier));

        for (String wiki_id : l_wiki_id) {
          // Add sameAs with wiki entity id
          model.add(
              newsEntity,
              ResourceFactory.createProperty("http://www.w3.org/2002/07/owl#sameAs"),
              ResourceFactory.createResource("http://www.wikidata.org/entity/" + wiki_id));
        }
      }

      // Make sure that the dump directory exists
      try {
        Files.createDirectories(outputDir);
      } catch (IOException e) {
        LOGGER.error(e.toString());
      }

      // Write output
      LOGGER.info("Writing annotations to file {}", outFile);
      try (final OutputStream outRdf =
          Files.newOutputStream(
              outFile, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING)) {
        RDFDataMgr.write(outRdf, model, RDFFormat.TURTLE_PRETTY);
      } catch (final IOException e) {
        LOGGER.error("Issue to write annotated file", e);
      }
    } catch (IOException e) {
      LOGGER.error(matchesFile.toString() + ": " + e.toString());
    }
  }
}
