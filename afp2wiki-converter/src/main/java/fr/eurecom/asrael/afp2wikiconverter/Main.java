package fr.eurecom.asrael.afp2wikiconverter;

import fr.eurecom.asrael.commons.utils.Configuration;
import java.io.IOException;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class Main {

  public static void main(final String... args) {
    try {
      new Afp2WikiConverter(Configuration.load()).run();
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }
}
