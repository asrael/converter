package fr.eurecom.asrael.commons.utils;

import org.apache.commons.codec.digest.DigestUtils;

import java.nio.charset.Charset;
import java.util.UUID;

public final class UUIDGenerator {
  private UUIDGenerator() {}

  /**
   * Generate an UUID from a seed.
   *
   * @param seed The seed used to generate the UUID
   * @return the generated UUID
   */
  public static String generateUUID(final String seed) {
    final String hash = DigestUtils.sha1Hex(seed);

    return UUID.nameUUIDFromBytes(hash.getBytes(Charset.forName("UTF-8"))).toString();
  }
}
