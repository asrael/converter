package fr.eurecom.asrael.commons.utils;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.apache.commons.io.FilenameUtils;

public class FileUtils {

  /**
   * @param path Original path without suffix
   * @return Unique path (possibly with a suffix) guaranteed to not exist
   */
  public static Path getUniquePath(final Path path) {
    int num = 0;
    final String pathNameWithoutExtension = FilenameUtils.removeExtension(path.toString());
    String uniquePathName = path.toString();
    Path uniquePath = Paths.get(uniquePathName);
    while (uniquePath.toFile().exists()) {
      uniquePathName =
          pathNameWithoutExtension + (num++) + "_" + FilenameUtils.getExtension(path.toString());
      uniquePath = Paths.get(uniquePathName);
    }
    return uniquePath;
  }
}
