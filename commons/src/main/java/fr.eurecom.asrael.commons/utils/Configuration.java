package fr.eurecom.asrael.commons.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Set;
import java.util.stream.Collectors;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.representer.Representer;

public class Configuration {
  // Common
  private String resourcesPath;
  private String dumpPath;
  private String schemasPath;

  // News Converter
  private String datasetName;
  private Set<String> forbiddenKeywords;
  private Set<String> forbiddenGenres;
  private Set<String> forbiddenSubjects;

  // Brat Annotator
  private String annotatorPath;
  private String annotatorFile;

  // Classification Converter
  private String classificationJsonPath;
  private String classificationXmlPath;

  // Clusters Converter
  private String clustersPath;

  // Afp2Wiki Converter
  private String afp2WikiMatchesPath;

  public static Configuration load() throws IOException {
    File file = new File("props/config.yml");
    if (!file.exists() || !file.isFile() || !file.canRead()) {
      return load("props/default.yml");
    }
    return load(file.getPath());
  }

  public static Configuration load(String configFilePath) throws IOException {
    Configuration config;
    try (final InputStream input = new FileInputStream(configFilePath)) {
      Representer representer = new Representer();
      representer.getPropertyUtils().setSkipMissingProperties(true);
      config = new Yaml(representer).loadAs(input, Configuration.class);
      return config;
    }
  }

  public String getResourcesPath() {
    return resourcesPath;
  }

  public void setResourcesPath(String resourcesPath) {
    this.resourcesPath = resourcesPath;
  }

  public String getDumpPath() {
    return dumpPath;
  }

  public void setDumpPath(String dumpPath) {
    this.dumpPath = dumpPath;
  }

  public String getSchemasPath() {
    return schemasPath;
  }

  public void setSchemasPath(String schemasPath) {
    this.schemasPath = schemasPath;
  }

  public String getDatasetName() {
    return datasetName;
  }

  public void setDatasetName(String datasetName) {
    this.datasetName = datasetName;
  }

  public Set<String> getForbiddenKeywords() {
    return forbiddenKeywords;
  }

  public void setForbiddenKeywords(Set<String> forbiddenKeywords) {
    this.forbiddenKeywords =
        forbiddenKeywords.stream().map(String::toLowerCase).collect(Collectors.toSet());
  }

  public Set<String> getForbiddenGenres() {
    return forbiddenGenres;
  }

  public void setForbiddenGenres(Set<String> forbiddenGenres) {
    this.forbiddenGenres =
        forbiddenGenres.stream().map(String::toLowerCase).collect(Collectors.toSet());
  }

  public Set<String> getForbiddenSubjects() {
    return forbiddenSubjects;
  }

  public void setForbiddenSubjects(Set<String> forbiddenSubjects) {
    this.forbiddenSubjects =
        forbiddenSubjects.stream().map(String::toLowerCase).collect(Collectors.toSet());
  }

  public String getAnnotatorPath() {
    return annotatorPath;
  }

  public void setAnnotatorPath(String annotatorPath) {
    this.annotatorPath = annotatorPath;
  }

  public String getAnnotatorFile() {
    return annotatorFile;
  }

  public void setAnnotatorFile(String annotatorFile) {
    this.annotatorFile = annotatorFile;
  }

  public String getClassificationJsonPath() {
    return classificationJsonPath;
  }

  public void setClassificationJsonPath(String classificationJsonPath) {
    this.classificationJsonPath = classificationJsonPath;
  }

  public String getClassificationXmlPath() {
    return classificationXmlPath;
  }

  public void setClassificationXmlPath(String classificationXmlPath) {
    this.classificationXmlPath = classificationXmlPath;
  }

  public String getClustersPath() {
    return clustersPath;
  }

  public void setAfp2WikiMatchesPath(String afp2WikiMatchesPath) {
    this.afp2WikiMatchesPath = afp2WikiMatchesPath;
  }

  public String getAfp2WikiMatchesPath() {
    return afp2WikiMatchesPath;
  }

  public void setClustersPath(String clustersPath) {
    this.clustersPath = clustersPath;
  }

  @Override
  public String toString() {
    return "Configuration{"
        + "resourcesPath='"
        + resourcesPath
        + '\''
        + ", dumpPath='"
        + dumpPath
        + '\''
        + ", schemasPath='"
        + schemasPath
        + '\''
        + ", datasetName='"
        + datasetName
        + '\''
        + ", forbiddenKeywords="
        + forbiddenKeywords
        + ", forbiddenGenres="
        + forbiddenGenres
        + ", forbiddenSubjects="
        + forbiddenSubjects
        + ", annotatorPath='"
        + annotatorPath
        + '\''
        + ", annotatorFile='"
        + annotatorFile
        + '\''
        + ", classificationJsonPath='"
        + classificationJsonPath
        + '\''
        + ", classificationXmlPath='"
        + classificationXmlPath
        + '\''
        + ", clustersPath='"
        + clustersPath
        + ", afp2WikiMatchesPath='"
        + afp2WikiMatchesPath
        + '\''
        + '}';
  }
}
