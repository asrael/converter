package fr.eurecom.asrael.commons.datatypes;

import fr.eurecom.asrael.commons.utils.UUIDGenerator;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.vocabulary.DC_11;
import org.apache.jena.vocabulary.RDF;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class NewsArticle {
  private String id;
  private String dateline;
  private final Map<String, String> headlines;
  private LocalDateTime dateCreated;
  private LocalDateTime datePublished;
  private LocalDateTime dateModified;
  private final Map<String, String> descriptions;
  private String slug;
  private Set<String> subjects;
  private String genre;
  private String language;
  private String country;
  private String city;
  private Set<String> keywords;
  private Set<ImageObject> associatedMedias;
  public static final String BASE_URI = "http://asrael.eurecom.fr/news/";

  /** Default constructor. */
  public NewsArticle() {
    this.id = "";
    this.dateline = "";
    this.headlines = new HashMap<>();
    this.dateCreated = LocalDateTime.of(0, Month.JANUARY, 1, 0, 0, 0);
    this.datePublished = LocalDateTime.of(0, Month.JANUARY, 1, 0, 0, 0);
    this.dateModified = LocalDateTime.of(0, Month.JANUARY, 1, 0, 0, 0);
    this.descriptions = new HashMap<>();
    this.slug = "";
    this.subjects = new HashSet<>();
    this.genre = "";
    this.language = "";
    this.country = "";
    this.city = "";
    this.keywords = new HashSet<>();
    this.associatedMedias = new HashSet<>();
  }

  public final String getId() {
    return this.id;
  }

  public final void setId(final String newId) {
    this.id = newId;
  }

  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  public String getSlug() {
    return slug;
  }

  public void setSlug(String slug) {
    this.slug = slug;
  }

  public void setSubjects(Set<String> subjects) {
    this.subjects.clear();
    this.subjects.addAll(subjects);
  }

  public Set<String> getSubjects() {
    return this.subjects;
  }

  public void addSubject(String subject) {
    this.subjects.add(subject);
  }

  public String getGenre() {
    return genre;
  }

  public void setGenre(String genre) {
    this.genre = genre;
  }

  public Map<String, String> getDescriptions() {
    return Collections.unmodifiableMap(this.descriptions);
  }

  public final void setDescription(final Map<String, String> descriptions) {
    this.descriptions.clear();
    this.descriptions.putAll(descriptions);
  }

  public void addDescription(String lang, String description) {
    if (this.descriptions.containsKey(lang)) {
      final String tmp = this.descriptions.get(lang);
      this.descriptions.remove(lang);
      this.descriptions.put(lang, tmp + ' ' + lang.trim());
    } else {
      this.descriptions.put(lang, description.trim());
    }
  }

  public LocalDateTime getDateModified() {
    return dateModified;
  }

  public void setDateModified(LocalDateTime dateModified) {
    this.dateModified = dateModified;
  }

  public LocalDateTime getDatePublished() {
    return datePublished;
  }

  public void setDatePublished(LocalDateTime datePublished) {
    this.datePublished = datePublished;
  }

  public LocalDateTime getDateCreated() {
    return dateCreated;
  }

  public void setDateCreated(LocalDateTime dateCreated) {
    this.dateCreated = dateCreated;
  }

  public String getDateline() {
    return dateline;
  }

  public final void setDateline(String dateline) {
    this.dateline = dateline;
  }

  public Map<String, String> getHeadlines() {
    return Collections.unmodifiableMap(this.headlines);
  }

  public final void setHeadlines(final Map<String, String> headlines) {
    this.headlines.clear();
    this.headlines.putAll(headlines);
  }

  public void addHeadline(String lang, String headline) {
    if (this.headlines.containsKey(lang)) {
      final String tmp = this.headlines.get(lang);
      this.headlines.remove(lang);
      this.headlines.put(lang, tmp + ' ' + lang.trim());
    } else {
      this.headlines.put(lang, headline.trim());
    }
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public void setKeywords(Set<String> keywords) {
    this.keywords.clear();
    this.keywords.addAll(keywords);
  }

  public Set<String> getKeywords() {
    return this.keywords;
  }

  public void addKeyword(String keyword) {
    this.keywords.add(keyword);
  }

  public Set<ImageObject> getAssociatedMedias() {
    return this.associatedMedias;
  }

  public void addAssociatedMedia(ImageObject media) {
    this.associatedMedias.add(media);
  }

  /**
   * Create the RDF model of the current news.
   *
   * @return A RDF model corresponding to a news
   */
  public final Model getRDFModel() {
    final Model model = ModelFactory.createDefaultModel();
    final Map<String, String> prefixes = new HashMap<>();

    prefixes.put("rdf", RDF.getURI());
    prefixes.put("dc", "http://purl.org/dc/elements/1.1/");
    prefixes.put("schema", "http://schema.org/");
    prefixes.put("xsd", "http://www.w3.org/2001/XMLSchema#");
    prefixes.put("rnews", "http://iptc.org/std/rNews/2011-10-07#");

    model.setNsPrefixes(prefixes);

    final Resource entity = ResourceFactory.createResource(this.getURI());

    model.add(
        entity,
        RDF.type,
        ResourceFactory.createResource("http://iptc.org/std/rNews/2011-10-07#Article"));

    // Identifier
    model.add(
        entity,
        ResourceFactory.createProperty("http://iptc.org/std/rNews/2011-10-07#identifier"),
        ResourceFactory.createPlainLiteral(this.id));

    // Dateline
    if (!this.dateline.isEmpty()) {
      model.add(
          entity,
          ResourceFactory.createProperty("http://iptc.org/std/rNews/2011-10-07#dateline"),
          ResourceFactory.createPlainLiteral(this.dateline));
    }

    // Headline
    for (final Map.Entry<String, String> entry : this.headlines.entrySet()) {
      model.add(
          entity,
          ResourceFactory.createProperty("http://iptc.org/std/rNews/2011-10-07#headline"),
          ResourceFactory.createLangLiteral(entry.getValue(), entry.getKey()));
    }

    // Date created
    model.add(
        entity,
        ResourceFactory.createProperty("http://iptc.org/std/rNews/2011-10-07#dateCreated"),
        ResourceFactory.createTypedLiteral(
            this.dateCreated.format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'")),
            XSDDatatype.XSDdateTime));

    // Date published
    model.add(
        entity,
        ResourceFactory.createProperty("http://iptc.org/std/rNews/2011-10-07#datePublished"),
        ResourceFactory.createTypedLiteral(
            this.datePublished.format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'")),
            XSDDatatype.XSDdateTime));

    // Content reference time
    model.add(
        entity,
        ResourceFactory.createProperty("http://schema.org/contentReferenceTime"),
        ResourceFactory.createTypedLiteral(
            this.datePublished.format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'")),
            XSDDatatype.XSDdateTime));

    // Date modified
    model.add(
        entity,
        ResourceFactory.createProperty("http://iptc.org/std/rNews/2011-10-07#dateModified"),
        ResourceFactory.createTypedLiteral(
            this.dateModified.format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'")),
            XSDDatatype.XSDdateTime));

    // Description
    if (!this.descriptions.isEmpty()) {
      for (final Map.Entry<String, String> entry : this.descriptions.entrySet()) {
        model.add(
            entity,
            ResourceFactory.createProperty("http://iptc.org/std/rNews/2011-10-07#articleBody"),
            ResourceFactory.createLangLiteral(entry.getValue(), entry.getKey()));
      }
    }

    // Slug
    if (!this.slug.isEmpty()) {
      model.add(
          entity,
          ResourceFactory.createProperty("http://iptc.org/std/rNews/2011-10-07#slug"),
          ResourceFactory.createPlainLiteral(this.slug));
    }

    // Subjects
    for (final String subject : this.subjects) {
      model.add(entity, DC_11.subject, ResourceFactory.createResource(subject));
    }

    // Keywords
    for (final String keyword : this.keywords) {
      model.add(
          entity,
          ResourceFactory.createProperty("http://schema.org/keywords"),
          ResourceFactory.createPlainLiteral(keyword));
    }

    // Associated medias
    for (final ImageObject media : this.associatedMedias) {
      if (!media.getUrl().isEmpty()) {
        model.add(media.getRDFModel());
        model.add(
            entity,
            ResourceFactory.createProperty("http://iptc.org/std/rNews/2011-10-07#associatedMedia"),
            ResourceFactory.createResource(media.getUrl()));
      }
    }

    // Genre
    if (!this.genre.isEmpty()) {
      model.add(
          entity,
          ResourceFactory.createProperty("http://iptc.org/std/rNews/2011-10-07#genre"),
          ResourceFactory.createPlainLiteral(this.genre));
    }

    // Language
    model.add(
        entity,
        ResourceFactory.createProperty("http://iptc.org/std/rNews/2011-10-07#inLanguage"),
        ResourceFactory.createPlainLiteral(this.language));

    // Content location
    model.add(
        entity,
        ResourceFactory.createProperty("http://schema.org/contentLocation"),
        ResourceFactory.createResource(
            NewsArticle.BASE_URI + UUIDGenerator.generateUUID(this.id) + "/contentLocation"));

    model.add(
        ResourceFactory.createResource(
            NewsArticle.BASE_URI + UUIDGenerator.generateUUID(this.id) + "/contentLocation"),
        ResourceFactory.createProperty("http://schema.org/address"),
        ResourceFactory.createResource(
            NewsArticle.BASE_URI
                + UUIDGenerator.generateUUID(this.id)
                + "/contentLocation/address"));

    model.add(
        ResourceFactory.createResource(
            NewsArticle.BASE_URI
                + UUIDGenerator.generateUUID(this.id)
                + "/contentLocation/address"),
        ResourceFactory.createProperty("http://schema.org/addressCountry"),
        ResourceFactory.createPlainLiteral(this.country));

    model.add(
        ResourceFactory.createResource(
            NewsArticle.BASE_URI
                + UUIDGenerator.generateUUID(this.id)
                + "/contentLocation/address"),
        ResourceFactory.createProperty("http://schema.org/addressLocality"),
        ResourceFactory.createPlainLiteral(this.city));

    return model;
  }

  public final String getURI() {
    return NewsArticle.BASE_URI + UUIDGenerator.generateUUID(this.id);
  }

  @Override
  public final boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }

    if ((obj == null) || (this.getClass() != obj.getClass())) {
      return false;
    }

    final NewsArticle news = (NewsArticle) obj;

    if (!this.id.equals(news.id)) {
      return false;
    }
    if (!this.dateline.equals(news.dateline)) {
      return false;
    }
    if (!this.headlines.equals(news.headlines)) {
      return false;
    }
    if (!this.dateCreated.equals(news.dateCreated)) {
      return false;
    }
    if (!this.datePublished.equals(news.datePublished)) {
      return false;
    }
    if (!this.dateModified.equals(news.dateModified)) {
      return false;
    }
    if (!this.descriptions.equals(news.descriptions)) {
      return false;
    }
    if (!this.slug.equals(news.slug)) {
      return false;
    }
    if (!this.subjects.equals(news.subjects)) {
      return false;
    }
    if (!this.genre.equals(news.genre)) {
      return false;
    }
    if (!this.language.equals(news.language)) {
      return false;
    }
    if (!this.keywords.equals(news.keywords)) {
      return false;
    }
    return this.associatedMedias.equals(news.associatedMedias);
  }

  @Override
  public final int hashCode() {
    int result = this.id.hashCode();

    result = 31 * (result + this.dateline.hashCode());
    result = 31 * (result + this.headlines.hashCode());
    result = 31 * (result + this.dateCreated.hashCode());
    result = 31 * (result + this.datePublished.hashCode());
    result = 31 * (result + this.dateModified.hashCode());
    result = 31 * (result + this.descriptions.hashCode());
    result = 31 * (result + this.slug.hashCode());
    result = 31 * (result + this.subjects.hashCode());
    result = 31 * (result + this.genre.hashCode());
    result = 31 * (result + this.language.hashCode());
    result = 31 * (result + this.keywords.hashCode());
    result = 31 * (result + this.associatedMedias.hashCode());

    return result;
  }

  @Override
  public final String toString() {
    return "NewsArticle{"
        + "id='"
        + this.id
        + '\''
        + ", dateline='"
        + this.dateline
        + '\''
        + ", headlines='"
        + this.headlines
        + '\''
        + ", dateCreated='"
        + this.dateCreated
        + '\''
        + ", datePublished='"
        + this.datePublished
        + '\''
        + ", dateModified='"
        + this.dateModified
        + '\''
        + ", description='"
        + this.descriptions
        + '\''
        + ", slug='"
        + this.slug
        + '\''
        + ", subjects='"
        + this.subjects
        + '\''
        + ", genre='"
        + this.genre
        + '\''
        + ", language='"
        + this.language
        + '\''
        + ", keywords='"
        + this.keywords
        + '\''
        + ", keywords='"
        + this.associatedMedias
        + '\''
        + '}';
  }
}
