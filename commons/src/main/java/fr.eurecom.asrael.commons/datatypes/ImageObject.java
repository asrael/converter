package fr.eurecom.asrael.commons.datatypes;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.vocabulary.RDF;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ImageObject {
  private String url;
  private int width;
  private int height;
  private String copyrightHolder;
  private String sourceOrganization;
  private final Map<String, String> descriptions;

  /** Default constructor. */
  public ImageObject() {
    this.url = "";
    this.width = 0;
    this.height = 0;
    this.copyrightHolder = "";
    this.sourceOrganization = "";
    this.descriptions = new HashMap<>();
  }

  public final String getUrl() {
    return url;
  }

  public final void setUrl(final String url) {
    this.url = url;
  }

  public final int getWidth() {
    return width;
  }

  public final void setWidth(final int width) {
    this.width = width;
  }

  public final int getHeight() {
    return height;
  }

  public final void setHeight(final int height) {
    this.height = height;
  }

  public final String getCopyrightHolder() {
    return copyrightHolder;
  }

  public final void setCopyrightHolder(final String copyrightHolder) {
    this.copyrightHolder = copyrightHolder;
  }

  public final String getSourceOrganization() {
    return sourceOrganization;
  }

  public final void setSourceOrganization(final String sourceOrganization) {
    this.sourceOrganization = sourceOrganization;
  }

  public Map<String, String> getDescriptions() {
    return Collections.unmodifiableMap(descriptions);
  }

  public final void setDescription(final Map<String, String> descriptions) {
    this.descriptions.clear();
    this.descriptions.putAll(descriptions);
  }

  public void addDescription(String lang, String description) {
    if (this.descriptions.containsKey(lang)) {
      final String tmp = this.descriptions.get(lang);
      this.descriptions.remove(lang);
      this.descriptions.put(lang, tmp + ' ' + lang.trim());
    } else {
      this.descriptions.put(lang, description.trim());
    }
  }

  /**
   * Create the RDF model of the current news.
   *
   * @return A RDF model corresponding to a news
   */
  public final Model getRDFModel() {
    final Model model = ModelFactory.createDefaultModel();
    final Map<String, String> prefixes = new HashMap<>();

    prefixes.put("rdf", RDF.getURI());
    prefixes.put("dc", "http://purl.org/dc/elements/1.1/");
    prefixes.put("schema", "http://schema.org/");
    prefixes.put("xsd", "http://www.w3.org/2001/XMLSchema#");
    prefixes.put("rnews", "http://iptc.org/std/rNews/2011-10-07#");

    model.setNsPrefixes(prefixes);

    if (this.url.isEmpty()) {
      return model;
    }

    model.add(
        ResourceFactory.createResource(this.url),
        RDF.type,
        ResourceFactory.createResource("http://iptc.org/std/rNews/2011-10-07#ImageObject"));

    // Width
    if (this.width > 0) {
      model.add(
          ResourceFactory.createResource(this.url),
          ResourceFactory.createProperty("http://iptc.org/std/rNews/2011-10-07#width"),
          ResourceFactory.createPlainLiteral(String.valueOf(this.width)));
    }

    // Height
    if (this.height > 0) {
      model.add(
          ResourceFactory.createResource(this.url),
          ResourceFactory.createProperty("http://iptc.org/std/rNews/2011-10-07#height"),
          ResourceFactory.createPlainLiteral(String.valueOf(this.height)));
    }

    // Copyright holder
    if (!this.copyrightHolder.isEmpty()) {
      model.add(
          ResourceFactory.createResource(this.url),
          ResourceFactory.createProperty("http://iptc.org/std/rNews/2011-10-07#copyrightHolder"),
          ResourceFactory.createPlainLiteral(this.copyrightHolder));
    }

    // Source organization
    if (!this.sourceOrganization.isEmpty()) {
      model.add(
          ResourceFactory.createResource(this.url),
          ResourceFactory.createProperty("http://iptc.org/std/rNews/2011-10-07#sourceOrganization"),
          ResourceFactory.createPlainLiteral(this.sourceOrganization));
    }

    // Description
    if (!this.descriptions.isEmpty()) {
      for (final Map.Entry<String, String> entry : this.descriptions.entrySet()) {
        model.add(
            ResourceFactory.createResource(this.url),
            ResourceFactory.createProperty("http://iptc.org/std/rNews/2011-10-07#description"),
            ResourceFactory.createLangLiteral(entry.getValue(), entry.getKey()));
      }
    }

    return model;
  }

  @Override
  public final boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }

    if ((obj == null) || (this.getClass() != obj.getClass())) {
      return false;
    }

    final ImageObject image = (ImageObject) obj;

    if (!this.url.equals(image.url)) {
      return false;
    }
    if (this.width != image.width) {
      return false;
    }
    if (this.height != image.height) {
      return false;
    }
    if (!this.copyrightHolder.equals(image.copyrightHolder)) {
      return false;
    }
    if (!this.sourceOrganization.equals(image.sourceOrganization)) {
      return false;
    }
    return this.descriptions.equals(image.descriptions);
  }

  @Override
  public final int hashCode() {
    int result = this.url.hashCode();

    result = 31 * (result + this.width);
    result = 31 * (result + this.height);
    result = 31 * (result + this.copyrightHolder.hashCode());
    result = 31 * (result + this.sourceOrganization.hashCode());
    result = 31 * (result + this.descriptions.hashCode());

    return result;
  }

  @Override
  public final String toString() {
    return "NewsArticle{"
        + "url='"
        + this.url
        + '\''
        + ", width='"
        + this.width
        + '\''
        + ", height='"
        + this.height
        + '\''
        + ", copyrightHolder='"
        + this.copyrightHolder
        + '\''
        + ", sourceOrganization='"
        + this.sourceOrganization
        + '\''
        + ", description='"
        + this.descriptions
        + '\''
        + '}';
  }
}
