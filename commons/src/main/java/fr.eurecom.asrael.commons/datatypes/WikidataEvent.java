package fr.eurecom.asrael.commons.datatypes;

import fr.eurecom.asrael.commons.utils.UUIDGenerator;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.vocabulary.DC_11;
import org.apache.jena.vocabulary.OWL2;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class WikidataEvent {
  private String id;
  private final Map<String, String> titles;
  private final Map<String, String> description;
  private String url;
  private String website;
  private final List<String> tags;
  private String canonicalURL;
  private String sameAs;
  private LocalDateTime createdTime;
  private LocalDateTime modifiedTime;
  public static final String BASE_URI = "http://asrael.eurecom.fr/event/";

  private String wikidataEntity;
  private Map<String, String> properties;
  private List<Annotation> annotations;

  /** Default constructor. */
  public WikidataEvent() {
    this.id = "";
    this.titles = new HashMap<>();
    this.description = new HashMap<>();
    this.url = "";
    this.website = "";
    this.tags = new ArrayList<>();
    this.canonicalURL = "";
    this.sameAs = "";
    this.createdTime = LocalDateTime.of(0, Month.JANUARY, 1, 0, 0, 0);
    this.modifiedTime = LocalDateTime.of(0, Month.JANUARY, 1, 0, 0, 0);

    this.wikidataEntity = "";
    this.properties = new HashMap<>();
    this.annotations = new ArrayList<>();
  }

  public final String getId() {
    return this.id;
  }

  public final void setId(final String newId) {
    this.id = newId;
  }

  public Map<String, String> getTitles() {
    return Collections.unmodifiableMap(this.titles);
  }

  public final void setTitles(final Map<String, String> titles) {
    this.titles.clear();
    this.titles.putAll(titles);
  }

  public void addTitle(String lang, String title) {
    if (this.titles.containsKey(lang)) {
      final String tmp = this.titles.get(lang);
      this.titles.remove(lang);
      this.titles.put(lang, tmp + ' ' + lang.trim());
    } else {
      this.titles.put(lang, title.trim());
    }
  }

  public final void setDescription(final Map<String, String> newDescription) {
    this.description.putAll(newDescription);
  }

  /**
   * Add a description for a language.
   *
   * @param newLanguage The language corresponding to the description
   * @param newDescription New description to add
   */
  public final void addDescription(final String newLanguage, final String newDescription) {
    if (this.description.containsKey(newLanguage)) {
      final String tmp = this.description.get(newLanguage);

      this.description.remove(newLanguage);

      this.description.put(newLanguage, tmp + ' ' + newDescription.trim());
    } else {
      this.description.put(newLanguage, newDescription.trim());
    }
  }

  public final Map<String, String> getDescription() {
    return Collections.unmodifiableMap(this.description);
  }

  public final String getUrl() {
    return this.url;
  }

  public final void setUrl(final String newUrl) {
    this.url = newUrl;
  }

  public final String getWebsite() {
    return this.website;
  }

  public final void setWebsite(final String newWebsite) {
    this.website = newWebsite;
  }

  public final List<String> getTags() {
    return Collections.unmodifiableList(this.tags);
  }

  public final void setTags(final Collection<String> newTags) {
    this.tags.clear();
    this.tags.addAll(newTags);
  }

  public final String getCanonicalURL() {
    return this.canonicalURL;
  }

  public final void setCanonicalURL(final String newCanonicalURL) {
    this.canonicalURL = newCanonicalURL;
  }

  public final String getSameAs() {
    return this.sameAs;
  }

  public final void setSameAs(final String newSameAs) {
    this.sameAs = newSameAs;
  }

  public final LocalDateTime getCreatedTime() {
    return this.createdTime;
  }

  public final void setCreatedTime(final LocalDateTime newCreatedTime) {
    this.createdTime = newCreatedTime;
  }

  public final LocalDateTime getModifiedTime() {
    return this.modifiedTime;
  }

  public final void setModifiedTime(final LocalDateTime newModifiedTime) {
    this.modifiedTime = newModifiedTime;
  }

  /**
   * Add a new tag.
   *
   * @param newTag New tag to add
   */
  public final void addTag(final String newTag) {
    if (!this.tags.contains(newTag)) {
      this.tags.add(newTag);
    }
  }

  public String getWikidataEntity() {
    return wikidataEntity;
  }

  public void setWikidataEntity(String wikidataEntity) {
    this.wikidataEntity = wikidataEntity;
  }

  public Map<String, String> getProperties() {
    return properties;
  }

  public void setProperties(Map<String, String> properties) {
    this.properties = properties;
  }

  public void addProperty(String name, String value) {
    this.properties.put(name, value);
  }

  public void addAnnotation(Annotation ann) {
    this.annotations.add(ann);
  }

  /**
   * Create the RDF model of the current annotation.
   *
   * @return A RDF model corresponding to an annotation
   */
  public final Model getModel() {
    final Model model = ModelFactory.createDefaultModel();
    final Map<String, String> prefixes = new HashMap<>();
    final LocalDateTime today = LocalDateTime.now();

    prefixes.put("rdfs", RDFS.getURI());
    prefixes.put("rdf", RDF.getURI());
    prefixes.put("owl", OWL2.NS);
    prefixes.put("schema", "http://schema.org/");
    prefixes.put("time", "http://www.w3.org/2006/time#");
    prefixes.put("dc", "http://purl.org/dc/elements/1.1/");
    prefixes.put("xsd", "http://www.w3.org/2001/XMLSchema#");
    prefixes.put("asrael", "http://asrael.eurecom.fr/asrael#");
    prefixes.put("rnews", "http://iptc.org/std/rNews/2011-10-07#");
    prefixes.put("wd", "http://www.wikidata.org/entity/");
    prefixes.put("wdt", "http://www.wikidata.org/prop/direct/");

    model.setNsPrefixes(prefixes);

    model.add(
        ResourceFactory.createResource(
            WikidataEvent.BASE_URI + UUIDGenerator.generateUUID(this.canonicalURL)),
        RDF.type,
        ResourceFactory.createResource("http://schema.org/Event"));
    model.add(
        ResourceFactory.createResource(
            WikidataEvent.BASE_URI + UUIDGenerator.generateUUID(this.canonicalURL)),
        RDF.type,
        ResourceFactory.createResource("http://iptc.org/std/rNews/2011-10-07#Concept"));
    model.add(
        ResourceFactory.createResource(
            WikidataEvent.BASE_URI + UUIDGenerator.generateUUID(this.canonicalURL)),
        RDF.type,
        ResourceFactory.createResource("http://www.wikidata.org/entity/" + this.wikidataEntity));

    for (final Map.Entry<String, String> entry : this.titles.entrySet()) {
      model.add(
          ResourceFactory.createResource(
              WikidataEvent.BASE_URI + UUIDGenerator.generateUUID(this.canonicalURL)),
          RDFS.label,
          ResourceFactory.createLangLiteral(entry.getValue(), entry.getKey()));
    }

    model.add(
        ResourceFactory.createResource(
            WikidataEvent.BASE_URI + UUIDGenerator.generateUUID(this.canonicalURL)),
        DC_11.identifier,
        ResourceFactory.createPlainLiteral(this.id));

    if (!this.website.isEmpty()) {
      String finalWebsite = this.website;
      if (!finalWebsite.startsWith("http://") && !finalWebsite.startsWith("https://")) {
        finalWebsite = "http://" + finalWebsite;
      }
      model.add(
          ResourceFactory.createResource(
              WikidataEvent.BASE_URI + UUIDGenerator.generateUUID(this.canonicalURL)),
          RDFS.seeAlso,
          ResourceFactory.createResource(finalWebsite.replaceAll(" ", "")));
    }

    if (!this.url.isEmpty()) {
      String finalUrl = this.url;
      if (!finalUrl.startsWith("http://") && !finalUrl.startsWith("https://")) {
        finalUrl = "http://" + finalUrl;
      }
      model.add(
          ResourceFactory.createResource(
              WikidataEvent.BASE_URI + UUIDGenerator.generateUUID(this.canonicalURL)),
          RDFS.seeAlso,
          ResourceFactory.createResource(finalUrl.replaceAll(" ", "")));
    }

    if (!this.sameAs.isEmpty()) {
      model.add(
          ResourceFactory.createResource(
              WikidataEvent.BASE_URI + UUIDGenerator.generateUUID(this.canonicalURL)),
          OWL2.sameAs,
          ResourceFactory.createResource(this.sameAs));
    }

    if (!this.description.isEmpty()) {
      for (final Map.Entry<String, String> entry : this.description.entrySet()) {
        model.add(
            ResourceFactory.createResource(
                WikidataEvent.BASE_URI + UUIDGenerator.generateUUID(this.canonicalURL)),
            DC_11.description,
            ResourceFactory.createLangLiteral(entry.getValue(), entry.getKey()));
      }
    }

    model.add(
        ResourceFactory.createResource(
            WikidataEvent.BASE_URI + UUIDGenerator.generateUUID(this.canonicalURL)),
        ResourceFactory.createProperty("http://purl.org/dc/elements/1.1/issued"),
        ResourceFactory.createTypedLiteral(
            today.truncatedTo(ChronoUnit.SECONDS).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)
                + "Z",
            XSDDatatype.XSDdateTime));

    if (this.createdTime != null) {
      if (!this.createdTime.equals(LocalDateTime.of(0, Month.JANUARY, 1, 0, 0, 0))) {
        model.add(
            ResourceFactory.createResource(
                WikidataEvent.BASE_URI + UUIDGenerator.generateUUID(this.canonicalURL)),
            ResourceFactory.createProperty("http://purl.org/dc/elements/1.1/created"),
            ResourceFactory.createTypedLiteral(
                this.createdTime
                        .truncatedTo(ChronoUnit.SECONDS)
                        .format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)
                    + "Z",
                XSDDatatype.XSDdateTime));
      } else {
        // Default creation time = today
        model.add(
            ResourceFactory.createResource(
                WikidataEvent.BASE_URI + UUIDGenerator.generateUUID(this.canonicalURL)),
            ResourceFactory.createProperty("http://purl.org/dc/elements/1.1/created"),
            ResourceFactory.createTypedLiteral(
                today.truncatedTo(ChronoUnit.SECONDS).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)
                    + "Z",
                XSDDatatype.XSDdateTime));
      }
    }

    if (this.modifiedTime != null) {
      if (!this.modifiedTime.equals(LocalDateTime.of(0, Month.JANUARY, 1, 0, 0, 0))) {
        model.add(
            ResourceFactory.createResource(
                WikidataEvent.BASE_URI + UUIDGenerator.generateUUID(this.canonicalURL)),
            ResourceFactory.createProperty("http://purl.org/dc/elements/1.1/modified"),
            ResourceFactory.createTypedLiteral(
                this.modifiedTime
                        .truncatedTo(ChronoUnit.SECONDS)
                        .format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)
                    + "Z",
                XSDDatatype.XSDdateTime));
      } else {
        // Default modification time = today
        model.add(
            ResourceFactory.createResource(
                WikidataEvent.BASE_URI + UUIDGenerator.generateUUID(this.canonicalURL)),
            ResourceFactory.createProperty("http://purl.org/dc/elements/1.1/modified"),
            ResourceFactory.createTypedLiteral(
                today.truncatedTo(ChronoUnit.SECONDS).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)
                    + "Z",
                XSDDatatype.XSDdateTime));
      }
    }

    for (final String tag : this.tags) {
      model.add(
          ResourceFactory.createResource(
              WikidataEvent.BASE_URI + UUIDGenerator.generateUUID(this.canonicalURL)),
          DC_11.subject,
          ResourceFactory.createPlainLiteral(tag));
    }

    for (final Map.Entry<String, String> entry : this.properties.entrySet()) {
      model.add(
          ResourceFactory.createResource(
              WikidataEvent.BASE_URI + UUIDGenerator.generateUUID(this.canonicalURL)),
          ResourceFactory.createProperty("http://www.wikidata.org/prop/direct/" + entry.getKey()),
          ResourceFactory.createPlainLiteral(entry.getValue()));
    }

    for (final Annotation annotation : this.annotations) {
      model.add(annotation.getModel());
    }

    return model;
  }
}
