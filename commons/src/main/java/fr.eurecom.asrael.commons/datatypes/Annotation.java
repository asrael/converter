package fr.eurecom.asrael.commons.datatypes;

import fr.eurecom.asrael.commons.utils.UUIDGenerator;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.vocabulary.RDF;

import java.util.HashMap;
import java.util.Map;

public class Annotation {
  private static final String BASE_URI = "http://asrael.eurecom.fr/annotation/";
  private String body;
  private String property;
  private String propertyLabel;
  private String source;
  private int startPosition;
  private int endPosition;
  private String canonicalURL;

  public void setBody(String body) {
    this.body = body;
  }

  public void setProperty(String property) {
    this.property = property;
  }

  public void setPropertyLabel(String propertyLabel) {
    this.propertyLabel = propertyLabel;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public void setStartPosition(int startPosition) {
    this.startPosition = startPosition;
  }

  public void setEndPosition(int endPosition) {
    this.endPosition = endPosition;
  }

  /**
   * Create the RDF model of the current annotation.
   *
   * @return A RDF model corresponding to an annotation
   */
  public final Model getModel() {
    final Model model = ModelFactory.createDefaultModel();
    final Map<String, String> prefixes = new HashMap<>();

    prefixes.put("rdf", RDF.getURI());
    prefixes.put("xsd", "http://www.w3.org/2001/XMLSchema#");
    prefixes.put("oa", "http://www.w3.org/ns/oa#");
    prefixes.put("asrael", "http://asrael.eurecom.fr/asrael#");
    prefixes.put("wdt", "http://www.wikidata.org/prop/direct/");
    prefixes.put("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");

    model.setNsPrefixes(prefixes);

    model.add(
        ResourceFactory.createResource(
            Annotation.BASE_URI + UUIDGenerator.generateUUID(this.canonicalURL)),
        RDF.type,
        ResourceFactory.createResource("http://www.w3.org/ns/oa#Annotation"));

    // Body
    final Resource bodyRes =
        ResourceFactory.createResource(
            Annotation.BASE_URI
                + UUIDGenerator.generateUUID(this.canonicalURL)
                + "/body/"
                + this.property);
    model.add(
        bodyRes,
        ResourceFactory.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#value"),
        ResourceFactory.createPlainLiteral(this.body));
    model.add(
        bodyRes,
        ResourceFactory.createProperty("http://asrael.eurecom.fr/asrael#property"),
        ResourceFactory.createResource("http://www.wikidata.org/prop/direct/" + this.property));
    if (this.propertyLabel != null) {
      model.add(
            bodyRes,
            ResourceFactory.createProperty("http://www.w3.org/2000/01/rdf-schema#label"),
            ResourceFactory.createLangLiteral(this.propertyLabel, "en"));
    }
    model.add(
        ResourceFactory.createResource(
            Annotation.BASE_URI + UUIDGenerator.generateUUID(this.canonicalURL)),
        ResourceFactory.createProperty("http://www.w3.org/ns/oa#hasBody"),
        bodyRes);

    // Target
    Resource hasTarget = model.createResource();
    // Target -> hasSource
    hasTarget.addProperty(
        ResourceFactory.createProperty("http://www.w3.org/ns/oa#hasSource"),
        ResourceFactory.createResource(this.source));
    // Target -> hasSelector
    Resource hasSelector = model.createResource();
    hasSelector.addProperty(
        RDF.type, ResourceFactory.createResource("http://www.w3.org/ns/oa#TextPositionSelector"));
    hasSelector.addProperty(
        ResourceFactory.createProperty("http://www.w3.org/ns/oa#start"),
        ResourceFactory.createTypedLiteral(
            String.valueOf(this.startPosition), XSDDatatype.XSDnonNegativeInteger));
    hasSelector.addProperty(
        ResourceFactory.createProperty("http://www.w3.org/ns/oa#end"),
        ResourceFactory.createTypedLiteral(
            String.valueOf(this.endPosition), XSDDatatype.XSDnonNegativeInteger));
    // Add hasSelector to target
    hasTarget.addProperty(
        ResourceFactory.createProperty("http://www.w3.org/ns/oa#hasSelector"), hasSelector);
    // Add hasTarget to annotation
    model.add(
        ResourceFactory.createResource(
            Annotation.BASE_URI + UUIDGenerator.generateUUID(this.canonicalURL)),
        ResourceFactory.createProperty("http://www.w3.org/ns/oa#hasTarget"),
        hasTarget);

    return model;
  }

  public void setCanonicalURL(String canonicalURL) {
    this.canonicalURL = canonicalURL;
  }
}
